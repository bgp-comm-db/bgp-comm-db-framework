from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from datetime import datetime as DateTime
from sqlalchemy import desc
import pytest

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"

def test_create_an_include():
    with ConnectionSession() as cs:
        range1 = cs.session.query(StdCommRange).with_transformation(
            StdCommRange.from_value.transformer).order_by(
            desc(StdCommRange.datetime)).filter(
            StdCommRange.from_value <= StdValue(12732, 10030),
            StdCommRange.datetime <= DateTime.now(),
            StdCommRange.to_value == None).first()

        range2 = cs.session.query(StdCommRange).with_transformation(
            StdCommRange.from_value.transformer).order_by(
            desc(StdCommRange.datetime)).filter(
            StdCommRange.from_value <= StdValue(12732, 10040),
            StdCommRange.datetime <= DateTime.now(),
            StdCommRange.to_value == None).first()

        berlin = cs.session.query(Geographic).filter(
            Geographic.geo_short == 'EU-DE-*-BER-None').scalar()

        incl = StdComm.Include(std_comm=range1, scope=berlin,
                              std_comm_include=range2,
                              auto_detected=True, auto_detected_scope=True)

        cs.session.add(incl)
        cs.session.commit()

def test_add_field_to_existing_range_and_check_group_numbers ():
    with ConnectionSession() as cs:

        range_ = cs.session.query(StdCommRange).filter(
        StdCommRange.sgk == 30337).one()

        with pytest.raises(AttributeError):
            range_._group_numbers

        assert(0 in range_.group_numbers)

        assert(range_._group_numbers is not None)

        assert(1 in range_.group_numbers)
        assert(2 not in range_.group_numbers)

        field = StdCommField(1, 2, DateTime(2008,1,1), range_=range_,
            description="Test")

        cs.session.add(field)
        cs.session.commit()

        assert(0 in range_.group_numbers)
        assert(1 in range_.group_numbers)
        assert(2 in range_.group_numbers)
