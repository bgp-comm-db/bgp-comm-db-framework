from bgp_comm_db.db import *
from datetime import datetime as DateTime


def test_value_correct_value():
    std_value = StdValue(345, 754)
    assert std_value.value == 22610674

def test_value_correct_parse():
    std_value = StdValue("345:754")
    assert std_value.value == 22610674

def test_value_eq():
    std_value = StdValue(345, 754)
    assert std_value == 22610674
    assert std_value == "345:754"
    assert std_value == StdValue(345, 754)

def test_value_ne():
    std_value = StdValue(345, 754)
    assert not std_value != 22610674
    assert not std_value != "345:754"
    assert not std_value != StdValue(345, 754)

def test_value_le():
    std_value = StdValue(345, 754, DateTime(2004,3,4))
    assert std_value < 22610675
    assert std_value < "345:755"
    assert std_value < StdValue(345, 754, DateTime(2004,3,5))

def test_value_ge():
    std_value = StdValue(345, 754)
    assert std_value >= 22610674
    assert std_value >= "345:754"
    assert std_value >= StdValue(345, 754)

    assert std_value >= 22610673
    assert std_value >= "345:753"
