
EXPLAIN EXTENDED
================

+----+-------------+-----------+------+------------------------------------+------------------------------------+---------+-------+------+----------+-------------+
| id | select_type | table     | type | possible_keys                      | key                                | key_len | ref   | rows | filtered | Extra       |
+----+-------------+-----------+------+------------------------------------+------------------------------------+---------+-------+------+----------+-------------+
|  1 | SIMPLE      | std_range | ref  | asn_from_to_datetime_source_UNIQUE | asn_from_to_datetime_source_UNIQUE | 2       | const |    3 |   100.00 | Using where |
+----+-------------+-----------+------+------------------------------------+------------------------------------+---------+-------+------+----------+-------------+
1 row in set, 1 warning (0.00 sec)

get field (single)
==================

+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------------------+
| id | select_type | table       | type   | possible_keys                      | key     | key_len | ref                              | rows  | filtered | Extra                   |
+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------------------+
|  1 | PRIMARY     | <derived2>  | ALL    | NULL                               | NULL    | NULL    | NULL                             |  6957 |   100.00 | Using where             |
|  1 | PRIMARY     | std_range_1 | eq_ref | PRIMARY,fk_std_range_std_comm1_idx | PRIMARY | 8       | anon_1.std_field_std_rangeid     |     1 |   100.00 |                         |
|  1 | PRIMARY     | std_comm_1  | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_range_1.std_cmm_id  |     1 |   100.00 |                         |
|  2 | DERIVED     | <derived3>  | ALL    | NULL                               | NULL    | NULL    | NULL                             | 22094 |   100.00 | Using where             |
|  2 | DERIVED     | <derived4>  | ALL    | NULL                               | NULL    | NULL    | NULL                             | 22094 |   100.00 | Using where; Not exists |
|  4 | DERIVED     | std_field   | ALL    | PRIMARY,fk_std_field_std_comm2_idx | NULL    | NULL    | NULL                             | 22607 |   100.00 |                         |
|  4 | DERIVED     | std_comm    | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_field.std_comm_id   |     1 |   100.00 |                         |
|  3 | DERIVED     | std_field   | ALL    | PRIMARY,fk_std_field_std_comm2_idx | NULL    | NULL    | NULL                             | 22607 |   100.00 |                         |
|  3 | DERIVED     | std_comm    | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_field.std_comm_id   |     1 |   100.00 |                         |
+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------------------+
9 rows in set, 1 warning (8 min 13.40 sec)

get field (single) (without get latest)
=======================================

+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------+
| id | select_type | table       | type   | possible_keys                      | key     | key_len | ref                              | rows  | filtered | Extra       |
+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------+
|  1 | PRIMARY     | <derived2>  | ALL    | NULL                               | NULL    | NULL    | NULL                             |  6957 |   100.00 | Using where |
|  1 | PRIMARY     | std_range_1 | eq_ref | PRIMARY,fk_std_range_std_comm1_idx | PRIMARY | 8       | anon_1.std_field_std_range_id    |     1 |   100.00 |             |
|  1 | PRIMARY     | std_comm_1  | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_range_1.std_comm_id |     1 |   100.00 |             |
|  2 | DERIVED     | <derived3>  | ALL    | NULL                               | NULL    | NULL    | NULL                             | 22094 |   100.00 | Using where |
|  3 | DERIVED     | std_field   | ALL    | PRIMARY,fk_std_field_std_comm2_idx | NULL    | NULL    | NULL                             | 22277 |   100.00 |             |
|  3 | DERIVED     | std_comm    | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_field.std_comm_id   |     1 |   100.00 |             |
+----+-------------+-------------+--------+------------------------------------+---------+---------+----------------------------------+-------+----------+-------------+
6 rows in set, 1 warning (0.11 sec)

get range (three results)
=========================

+----+-------------+------------+--------+------------------------------------+---------+---------+--------------------------------+-------+----------+-------------------------+
| id | select_type | table      | type   | possible_keys                      | key     | key_len | ref                            | rows  | filtered | Extra                   |
+----+-------------+------------+--------+------------------------------------+---------+---------+--------------------------------+-------+----------+-------------------------+
|  1 | PRIMARY     | std_range  | ALL    | PRIMARY,fk_std_range_std_comm1_idx | NULL    | NULL    | NULL                           | 21530 |   100.00 | Using where             |
|  1 | PRIMARY     | <derived2> | ALL    | NULL                               | NULL    | NULL    | NULL                           | 20662 |   100.00 | Using where; Not exists |
|  1 | PRIMARY     | std_comm   | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_range.std_comm_id |     1 |   100.00 |                         |
|  2 | DERIVED     | std_range  | ALL    | PRIMARY,fk_std_range_std_comm1_idx | NULL    | NULL    | NULL                           | 21530 |   100.00 |                         |
|  2 | DERIVED     | std_comm   | eq_ref | PRIMARY                            | PRIMARY | 8       | bgp_comm.std_range.std_comm_id |     1 |   100.00 |                         |
+----+-------------+------------+--------+------------------------------------+---------+---------+--------------------------------+-------+----------+-------------------------+
5 rows in set, 1 warning (0.12 sec)