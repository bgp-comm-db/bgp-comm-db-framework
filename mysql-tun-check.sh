#!/bin/bash

ssh -S /tmp/port-tunnel-mysql-onyx.pid \
	-O check \
	-p 42512 \
	onyx.net.in.tum.de
