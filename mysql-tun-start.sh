#!/bin/bash

ssh -fnNT \
	-M -S /tmp/port-tunnel-mysql-onyx.pid \
	-p 42512 \
	-L 3306:localhost:3306 \
	onyx.net.in.tum.de
