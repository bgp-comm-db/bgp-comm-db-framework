Requirements
============

 + python3 (>= 3.4, tested 3.4)
 + SQL Alchemy (>= 0.9.10, tested 1.0.3) (requires: python >= 3.3)
 + MySQL Connector/Python 2 (tested 2.0.3)
     * `pip3 install --allow-external mysql-connector-python mysql-connector-python`
     * all other connectors which are supported by sqlalchemy are possible, but only this one is tested (config it then in `config/database.cfg`)
 + typeannotations (>= 0.1.1, >= commit [c1ef8573a029c172406bb8cc0ef3bcbe200a8226](https://github.com/raabf/typeannotations/tree/c1ef8573a029c172406bb8cc0ef3bcbe200a8226))
     * https://github.com/raabf/typeannotations
     * a submodule which is patched is included
 + progressbar (>= 2.4, >= commit [7363ca6da10046a59f91539b1353d5d01c8bcfbe](https://gitlab.com/raabf/python-progressbar/tree/7363ca6da10046a59f91539b1353d5d01c8bcfbe))
     * https://gitlab.com/raabf/python-progressbar
     * a submodule which is patched is included
 + python-dateutil (tested 1.5)
 + click (>= 4.0, tested 4.0)
 + sqlalchemy_utils (tested 0.30.1)
 + tabulate (tested 0.7.5)
 	* This is only used in `__main__` for some nice console output. If you do not call them it is not directly an requirement.
 + marshmallow (>= 2.0.0a1, tested 2.0.0b2)
