Installation
============

requirements
-------------

See `REQUIREMENTS.md` for a list of the requirements.

To install all requirements with minimal needed version just run:

	pip3 install -r requirements.txt

To install all requirements in their version they are tested just run:

	pip3 install -r requirements_tested.txt

Then fetch the submodules with

	git submodule update --init

install
-------

	python3 setup.py install

then the command `bgp-comm-db --help` is available. You should maybe run `chmod +xr -R` on the installed folder to allow every user to use the application.

**OR**

just run from the project root:

	python3 -m bgp_comm_db --help
