Contributing
============

Styleguides
-----------

 + [PEP8](https://www.python.org/dev/peps/pep-0008/)
 	* Ignore: E711
 + [PEP256](https://www.python.org/dev/peps/pep-0256/)
 + [Google Python Styleguide](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html)
 	* especially for [Comments](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html?showone=Comments#Comments)
 + For indent style, size, charset etc. see `.editorconfig`
