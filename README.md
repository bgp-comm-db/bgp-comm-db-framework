BGP Communities Database - Framework
====================================

[![build status](https://ci.gitlab.com/projects/3804/status.png?ref=master)](https://ci.gitlab.com/projects/3804?ref=master) [![Codacy Badge](https://www.codacy.com/project/badge/b4d9d1c3f771411ea377902e431c47c4)](https://www.codacy.com/app/raabf/bgp-comm-db-framework) [![codecov.io Badge](http://codecov.io/gitlab/bgp-comm-db/bgp-comm-db-framework/coverage.svg?branch=master)](https://codecov.io/gitlab/bgp-comm-db/bgp-comm-db-framework?branch=master)

This is a framework for accessing the BGP Communities Database.
With `SQLALchemy` it provides ORM mapper with many useful functions.

The developing stage is quite early, most documentation is missing and still some bugs can occur.

For install instructions see `INSTALL.md`.

settings are available in the `config/` directory.
