|         files         |                            source                            |     remarks     |
| --------------------- | ------------------------------------------------------------ | --------------- |
| ixps.csv              | http://www.datacentermap.com/ixps.html                       |                 |
| ixps.csv              | http://www.datacentermap.com/ixps.html                       |                 |
| "ases" subfolder      | http://bgp.potaroo.net/cidr/autnums.html                     |                 |
| subdivisions.csv      | https://www.ip2location.com/free/iso3166-2                   |                 |
| continents.csv        | self                                                         |                 |
| unlocode.csv          | http://www.unece.org/cefact/codesfortrade/codes_index.html   |                 |
| countries.csv         | http://dev.maxmind.com/geoip/legacy/codes/country_continent/ | some self added |
| iso3166-countries-with_continents.csv | http://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_by_continent_%28data_file%29 | |