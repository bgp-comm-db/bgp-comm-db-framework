"""
    shared environment over all modules
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import configparser
import logging
import os
import sqlalchemy.engine.url
import sys

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['proj_dir', 'engine', 'config', 'logging', 'Session']


def make_url(**kwargs):

    url_arg_part = ['port', 'database', 'drivername',
                    'host', 'username', 'user', 'password']
    url_arg_dbconfig = dict()
    url_opt_dbconfig = dict()
    for k, v in kwargs.items():
        if k in url_arg_part:
            if k == 'user':
                k = 'username'
            url_arg_dbconfig[k] = v
        else:
            url_opt_dbconfig[k] = v

    url = sqlalchemy.engine.url.URL(**url_arg_dbconfig)

    opt_str = ''
    # maybe use
    # http://docs.sqlalchemy.org/en/rel_1_0/core/engines.html#sqlalchemy.create_engine.params.connect_args
    # as an alternative?
    for k, v in url_opt_dbconfig.items():
        opt_str = opt_str + str(k) + '=' + str(v) + ','

    if opt_str:
        full_url = str(url) + '?' + opt_str[:-1]
    else:
        full_url = url

    return full_url

# Config

proj_dir = os.path.abspath(
    os.path.join(
        os.path.dirname(
            os.path.realpath(__file__)
        ), os.pardir
    )
)

config_paths = [
    os.path.join(os.path.expanduser('~'), '.config',
                 'bgp-comm-db', 'framework.cfg'),
    os.path.join(os.path.expanduser('~'), '.bgp-comm-db', 'framework.cfg'),
    '/etc/bgp-comm-db/framework.cfg',
    os.path.join(proj_dir, 'config', 'framework.cfg')]

config_path = next(cp for cp in config_paths if os.path.isfile(cp))

config_path_dir = os.path.dirname(config_path)

config = configparser.ConfigParser()
config.read(config_path)
config.set('PATHS', 'proj_dir', proj_dir)
config.set('PATHS', 'config_dir', config_path_dir)
config.set('PATHS', 'home_bgp-comm-db',
           os.path.join(os.path.expanduser('~'), '.bgp-comm-db'))
config.read(config.get('PATHS', 'mysql_db_config'))


# Logging

my_log_formatter = logging.Formatter("%(asctime)-15s %(levelname)-7s "
                                     "%(funcName)-20s %(message)s")

log_file = config['PATHS'].get('log_file', '/var/log/bgp_comm_db/all.log')

if not os.path.exists(os.path.dirname(log_file)):
    os.makedirs(os.path.dirname(log_file))

logging.basicConfig(
    filename=config['PATHS'].get('log_file', '/var/log/bgp_comm_db/all.log'),
    filemode='w',
    format="%(asctime)-15s %(levelname)-7s %(funcName)-20s %(message)s",
    level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
console_handler = logging.StreamHandler()
console_handler.setFormatter(my_log_formatter)
# logging.getLogger().addHandler(console_handler)


# Session

dbconfig = dict(config.items('client'))
engine = create_engine(make_url(**dbconfig))

# session_factory = sessionmaker(bind=engine)
# Session = scoped_session(session_factory)
Session = sessionmaker(bind=engine)
