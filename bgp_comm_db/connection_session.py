from abc import ABCMeta
from .environment import *

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['ConnectionSession']

class ConnectionSession(object):

    """A Class for safety getting an session with rollback functionality.

    It can also be inherited to add this functionality to a custom class.
    """

    def __init__(self, rollback_trans: bool=True):
        super(ConnectionSession, self).__init__()
        self.session = None
        self.connection = None
        self.trans = None

        self.rollback_trans = rollback_trans

    def __enter__(self):
        if self.rollback_trans:
            # connect to the database
            self.connection = engine.connect()

            # begin a non-ORM transaction
            self.trans = self.connection.begin()

            # bind an individual Session to the connection
            self.session = Session(bind=self.connection)
        else:
            self.session = Session()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        if self.rollback_trans:
            self.session.commit()

            # rollback - everything that happened with the
            # Session above (including calls to commit())
            # is rolled back.
            self.session.close()
            self.trans.rollback()
            self.connection.close()  # return connection to the Engine

        else:
            if exc_type:
                self.session.rollback()
            else:
                self.session.commit()
                self.session.close()
