def update_func(self, other, func):
    for k, v in other.items():
        try:
            self[k] = func(self[k], v)
        except KeyError as _:
            self[k] = v
