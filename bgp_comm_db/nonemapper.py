"""
Maps `None` to a special passed value
"""

from sqlalchemy.orm.attributes import InstrumentedAttribute
import sqlalchemy.ext.hybrid

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['NoneMapper']


class NoneMapper(object):

    @classmethod
    def property(cls, mapped_attr: str, mapping: object,
                 fdel=None, doc=None, mapping_allowed: bool=False):
        def getter_func(self):
            return cls.getter(mapping, getattr(self, mapped_attr))

        def setter_func(self, value):
            setattr(self, mapped_attr,
                    cls.setter(value, mapping, mapping_allowed))

        return property(fget=getter_func, fset=setter_func, fdel=fdel, doc=doc)

    @classmethod
    def hybrid_property(cls, mapping: object, mapped_attr: str,
                        fdel=None, mapping_allowed: bool=False):
        def getter_func(self):
            return cls.getter(mapping, getattr(self, mapped_attr))

        def setter_func(self, value):
            setattr(self, mapped_attr,
                    cls.setter(value, mapping, mapping_allowed))

        def expression_func(self):
            return cls.Comparator(mapping, getattr(self, mapped_attr))

        return sqlalchemy.ext.hybrid.hybrid_property(fget=getter_func,
                                                     fset=setter_func,
                                                     fdel=fdel,
                                                     expr=expression_func)

    @classmethod
    def getter(cls, mapping: object, mapped_attr: object):
        if mapped_attr == mapping:
            return None
        else:
            return mapped_attr

    @classmethod
    def setter(cls, value: object, mapping: object,
               mapping_allowed: bool=False):
        if (not mapping_allowed) and value == mapping:
            raise Exception("Value '%s' is reserved for None." % (mapping))
        elif value == None:
            return mapping
        else:
            return value

    class Comparator(sqlalchemy.ext.hybrid.Comparator):

        """docstring for NoneMapper.Comparator"""

        def __init__(self, mapping: object, mapped_attr: object):
            super(NoneMapper.Comparator, self).__init__(None)
            self.mapping = mapping
            self.mapped_attr = mapped_attr

        def __eq__(self, other):
            if other is None:
                return self.mapped_attr == self.mapping
            elif isinstance(other, NoneMapper.Comparator):
                return self.mapped_attr == other.mapped_attr
            else:
                return self.mapped_attr == other

        def __ne__(self, other):
            if other is None:
                return self.mapped_attr != self.mapping
            elif isinstance(other, NoneMapper.Comparator):
                return self.mapped_attr != other.mapped_attr
            else:
                return self.mapped_attr != other

        def operate(self, op, other):
            if isinstance(other, NoneMapper.Comparator):
                return op(self.mapped_attr, other.mapped_attr)
            else:
                return op(self.mapped_attr, other)

        def __clause_element__(self):
            if self.mapped_attr == self.mapping:
                return None
            else:
                return self.mapped_attr

        def __repr__(self):
            if self.mapped_attr == self.mapping:
                return repr(None)
            else:
                return repr(self.mapped_attr)


class NoneMapperRelationship(NoneMapper):

    """docstring for NoneMapperRelationship"""

    @classmethod
    def hybrid_property(cls, mapping: object, mapped_attr: str, fdel=None):

        def getter_func(self):
            return cls.getter(getattr(self, mapping),
                              getattr(self, mapped_attr))

        def setter_func(self, value):
            (new_mapping, new_mapped_attr) = cls.setter(
                value, getattr(self, mapping), getattr(self, mapped_attr))
            setattr(self, mapped_attr, new_mapped_attr)
            setattr(self, mapping, new_mapping)

        def expression_func(self):
            return cls.Comparator(getattr(self, mapping),
                                  getattr(self, mapped_attr))

        return sqlalchemy.ext.hybrid.hybrid_property(fget=getter_func,
                                                     fset=setter_func,
                                                     fdel=fdel,
                                                     expr=expression_func)

    @classmethod
    def getter(cls, mapping: object, mapped_attr: object):
        if mapping is None:
            return None
        else:
            return mapped_attr

    @classmethod
    def setter(cls, value: object, mapping: object, mapped_attr: object):
        if value is None:
            return (None, mapped_attr)
        else:
            return (mapping, value)

    class Comparator(NoneMapper.Comparator):

        def __init__(self, mapping: object, mapped_attr: object):
            super(NoneMapper.Relationship.Comparator, self).__init__(
                mapping, mapped_attr)

        def __eq__(self, other):
            if other is None:
                return self.mapping == None
            elif isinstance(other, NoneMapper.Relationship.Comparator):
                return self.mapped_attr == other.mapped_attr
            else:
                return self.mapped_attr == other

        def __ne__(self, other):
            if other is None:
                return self.mapping != None
            elif isinstance(other, NoneMapper.Relationship.Comparator):
                return self.mapped_attr != other.mapped_attr
            else:
                return self.mapped_attr != other

        def operate(self, op, other):
            if isinstance(other, NoneMapper.Relationship.Comparator):
                return op(self.mapped_attr, other.mapped_attr)
            else:
                return op(self.mapped_attr, other)

        def __clause_element__(self):
            if self.mapping == None:
                return None
            else:
                return self.mapped_attr

        def __repr__(self):
            if self.mapping == None:
                return repr(None)
            else:
                return repr(self.mapped_attr)


NoneMapper.Relationship = NoneMapperRelationship
del NoneMapperRelationship
