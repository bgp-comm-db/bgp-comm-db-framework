"""
"""

from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.tools.csv_insert import *
from bgp_comm_db.progressbar import Bar
from bgp_comm_db.progressbar import FormatLabel
from bgp_comm_db.progressbar import Percentage
from bgp_comm_db.progressbar import ProgressBar
from logging import critical
from logging import debug
from logging import error
from logging import info
from logging import warning
from sqlalchemy import and_
import csv
import itertools
import os
import pickle
import re

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['RegexDetection']


class RegexDetection(object):

    """docstring for RegexDetection"""

    @typechecked
    def __init__(self, description: str, value: StdValue, session):
        super(RegexDetection, self).__init__()
        self.description = description
        self.value = value
        self.session = session

    @typechecked
    def scope_as(self) -> [Ases]:
        """Try to find one or more ASN in the description."""

        re_as = re.compile(".*AS([\d]+).*", re.IGNORECASE | re.MULTILINE)

        l_asn = re_as.findall(self.description)

        l_as_obj = []

        for asn in l_asn:
            as_obj = Ases.get_at_datetime(self.session, asn,
                                          self.value.datetime)
            if as_obj is not None:
                l_as_obj.append(as_obj)

        return l_as_obj

    @typechecked
    def localpref_preference(self) -> optional(int):
        """Try to find the preference through regex from the description."""

        re_preference = re.compile(
            "(.*set.*to|.*of|.*=)\W+([0-9]*)(.*)", re.IGNORECASE)

        m_preference = re_preference.match(self.description)
        preference = None

        if m_preference:
            preference = m_preference.group(2)

        # Try to convert to an int
        try:
            int_preference = int(preference)
            preference = int_preference
        except (ValueError, TypeError):
            pass

        if not isinstance(preference, int):
            preference = None
            warning("Preference not correctly detected. description:",
                    self.description)

        return preference

    @typechecked
    def prepend_times_tuple(self) -> (
            optional(str), optional(union(str, int)), optional(str)):
        """Try to find the prepend times through regex from the description."""

        re_times_with_to = re.compile(
            "(.*)\s+([\d\w]*[\da-wy-zA-WY-Z_]+)"
            "[xX]?\s+(to|times)\s+(.*)", re.IGNORECASE)

        re_times = re.compile(
            "(.*)\s+([\d\w]*[\da-wy-zA-WY-Z_]+)"
            "[xX]?\s+(times)\s+(.*)", re.IGNORECASE)

        m_times = re_times_with_to.match(self.description)

        # string does not contain a number or variable like "Prepend to DE-CIX"
        re_times_unspecified = re.compile(
            "(.*)(\s+)(to.*|time.*)", re.IGNORECASE)

        if not m_times:
            m_times = re_times_unspecified.match(self.description)

        left = None
        times = None
        right = None

        if m_times:
            left = m_times.group(1)
            times = m_times.group(2)
            if m_times.lastindex >= 4:
                right = m_times.group(4)

            # If we have "times to" like in "Prepends 2 times to Cogent", the
            # regex re_times_with_to will match group 3 to "2 times" instead of
            # "2", so we had to do it again with no "to".
            if "time" in times:
                m_times = re_times.match(self.description)
                if m_times:
                    left = m_times.group(1)
                    times = m_times.group(2)

            # Check if the number is pronounced
            text_numb = [["zero", "null"], ["one", "once"], ["twice", "two"],
                         ["three"], ["four"], ["five"], ["six"], ["seven"],
                         ["eight"], ["nine"], ["ten"]]

            for i, ltext in enumerate(text_numb):
                for text in ltext:
                    if text in times:
                        # str(i) To allow that the for loop can go on
                        # (int is not iterable)
                        times = str(i)

            # Try to convert to an int
            try:
                int_times = int(times)
                times = int_times
            except (ValueError, TypeError):
                pass

        # Check if it do "not prepend"
        re_not_prepend = re.compile("(.*)not(\s+)prepend(.*)", re.IGNORECASE)
        m_not_prepend = re_not_prepend.match(self.description)

        if m_not_prepend:
            times = 0

        # Check if times is maybe an ASN
        if times == self.value.asn:
            # we assume here that he can left out the times because it is
            # single prepend. Something like "Prepend 15432 twice to" or
            # "Prepend 15342 3 times" should be detected by the regex above
            times = 1

        if isinstance(times, int) and times > 20:
            raise Exception("Times is too big to be realistic:",
                            self.value, self.description)

        return (left, times, self.description if right == None else right)

    @typechecked
    def prepend_times(self) -> optional(int):
        (_, times, _) = self.prepend_times_tuple()
        if isinstance(times, int):
            return times
        else:
            return None

    PEERTYPES_REGEX = {
        'customer': re.compile(".*customer.*", re.IGNORECASE),
        'non_customer': re.compile(
            ".*(non-customer|non customer).*", re.IGNORECASE),
        'internal': re.compile(".*internal.*", re.IGNORECASE),
        'internal_more_specific': re.compile(
            ".*(internal_more_specific|internal more specific).*",
            re.IGNORECASE),
        'special_purpose': re.compile(
            ".*(special_purpose|special purpose).*", re.IGNORECASE),
        'upstream': re.compile(".*(upstream|provider|uplink).*",
                               re.IGNORECASE),
        'global_': re.compile(".*global.*", re.IGNORECASE),
        'border': re.compile(".*border.*", re.IGNORECASE),
        'transit': re.compile(".*transit.*", re.IGNORECASE),
        'stub': re.compile(".*stub.*", re.IGNORECASE),
        'multihomed': re.compile(".*multihomed.*", re.IGNORECASE),
    }

    PEERTYPE_PEER_REGEX = re.compile(".*peer.*", re.IGNORECASE)

    def scope_peertype(self) -> optional([Peertype]):
        # peer
        # customer
        # non-customer
        # internal
        # internal_more_specific
        # special_purpose
        # upstream/provider
        # global
        # border
        # transit
        # stub
        # multihomed

        detected_types = []
        for s_peertype, regex in RegexDetection.PEERTYPES_REGEX.items():
            m_regex = regex.match(self.description)
            if m_regex:
                peertype = self.session.query(Peertype).filter(
                    Peertype.type_ == PeertypeEnum[s_peertype]).first()
                detected_types.append(peertype)

        if not detected_types:
            m_peer_regex = RegexDetection.PEERTYPE_PEER_REGEX.match(
                self.description)
            if m_peer_regex:
                peertype = self.session.query(Peertype).filter(
                    Peertype.type_ == PeertypeEnum.peer).first()
                detected_types.append(peertype)

        return detected_types

    def _skip_row(self, row, csv_config) -> bool:
        if (csv_config['options'].get('skip_blank_lines', False) and
                not any(row)):
            print("EMPTY LINE SKIP")
            return True
        for entry in csv_config['skip']:
            if bool(re.match(entry['regex'], row[entry['srow']])):
                return True
        return False

    def _includes_keywords(self, csv_config, row_tuple_list: [{}, []],
                           query_func) -> [Scope]:

        widgets = [FormatLabel('Checking %(comment)s: '), Percentage(), Bar()]
        pbar = ProgressBar(
            widgets=widgets, maxval=csv_config['maxval']).start()

        return_list = []

        i = 0
        for i, (row, list_regex) in enumerate(row_tuple_list):

            if self._skip_row(row, csv_config):
                continue

            any_match = False

            for regex in list_regex:

                match = regex.match(self.description)

                any_match = bool(match) or any_match

            if any_match:
                con = self.session.query(csv_config['classname']).filter(
                    query_func(row)).first()

                if con is not None:
                    return_list.append(con)
                else:
                    warning("%s is maybe missing in database" %
                            csv_config['comment_func_dict'](row))

            pbar.update(
                i + 1, comment=csv_config['comment_func_dict'](row))

        pbar.clear()

        return return_list

    def _includes_keywords_load(self, csv_config, list_regex) -> [({}, [])]:

        csv_file_basename = os.path.splitext(
            os.path.basename(
                csv_config['csv_file']))[0]

        data_file_dir_path = "/tmp/bgp_comm_db"
        data_file_path = data_file_dir_path + "/%s.data" % (csv_file_basename)

        widgets = [FormatLabel('Loading %(comment)s: '), Percentage(), Bar()]
        pbar = ProgressBar(
            widgets=widgets, maxval=csv_config['maxval']).start()

        if os.path.isfile(data_file_path):
            pbar.update(csv_config['maxval']/2,
                        comment="list from pickle file %s... "
                        "(ProgressBar will not change)" % (data_file_path))
            with open(data_file_path, 'rb') as data_file:
                data = pickle.load(data_file)
                return data

        return_list = []

        with open(csv_config['csv_file'], mode='r', closefd=True) as file:

            reader = csv.DictReader(file, **csv_config['reader_args'])

            i = 0
            for i, row in enumerate(reader):

                if self._skip_row(row, csv_config):
                    continue

                pbar.update(
                    i + 1, comment=csv_config['comment_func_dict'](row))

                regex_list = []

                for (re_func, opt) in list_regex:

                    try:
                        regex = re.compile(re_func(row), opt)
                    except Exception as e:
                        print(re_func(row), opt)
                        raise e

                    regex_list.append(regex)

                return_list.append((row, regex_list))

        if not os.path.exists(data_file_dir_path):
            os.makedirs(data_file_dir_path)

        with open(data_file_path, 'wb') as data_file:
            pickle.dump(return_list, data_file, pickle.HIGHEST_PROTOCOL)

        pbar.clear()

        return return_list

    SCOPE_IXP_TUPLE_LIST = None

    def scope_ixp(self) -> optional([Ixp]):

        csv_config = CsvInsert.config_ixps
        query_func = lambda row: Ixp.name == row['name']
        regex_func = lambda row: ("(^.*\W+|^)" + row['name'] + "($|\W+.*$)")
        regex_options = re.IGNORECASE | re.MULTILINE
        list_regex = [(regex_func, regex_options)]

        if RegexDetection.SCOPE_IXP_TUPLE_LIST is None:
            RegexDetection.SCOPE_IXP_TUPLE_LIST = self._includes_keywords_load(
                csv_config, list_regex)

        return self._includes_keywords(csv_config,
                                       RegexDetection.SCOPE_IXP_TUPLE_LIST,
                                       query_func)

    SCOPE_CONTINENT_TUPLE_LIST = None

    def _scope_continent(self) -> [Geographic]:
        csv_config = CsvInsert.config_continets
        query_func = lambda row: (and_(Geographic.continent == row['short2'],
                                       Geographic.country == None,
                                       Geographic.subdivision == None))
        regex_func1 = lambda row: ("(^.*\W+|^)" + row['name'] + "($|\W+.*$)")
        regex_func2 = lambda row: ("(^.*\W+|^)" + row['short2'] + "($|\W+.*$)")
        list_regex = [(regex_func1, re.IGNORECASE | re.MULTILINE),
                      (regex_func2, re.MULTILINE)]

        if RegexDetection.SCOPE_CONTINENT_TUPLE_LIST is None:
            RegexDetection.SCOPE_CONTINENT_TUPLE_LIST = (
                self._includes_keywords_load(csv_config, list_regex))

        return self._includes_keywords(
            csv_config, RegexDetection.SCOPE_CONTINENT_TUPLE_LIST, query_func)

    SCOPE_COUNTRY_TUPLE_LIST = None

    def _scope_country(self) -> [Geographic]:
        csv_config = CsvInsert.config_countries_iso3166
        query_func = lambda row: (and_(Geographic.country == row['iso2'],
                                       Geographic.subdivision == None,
                                       Geographic.location == None))
        regex_func1 = lambda row: ("(^.*\W+|^)(" + row['iso2'] + '|' +
                                   row['iso3'] + ")($|\W+.*$)")
        regex_func2 = lambda row: ("(^.*\W+|^)" + re.split(',', row['name'])[0]
                                   + "($|\W+.*$)")
        list_regex = [(regex_func1, re.MULTILINE),
                      (regex_func2, re.IGNORECASE | re.MULTILINE), ]

        if RegexDetection.SCOPE_COUNTRY_TUPLE_LIST is None:
            RegexDetection.SCOPE_COUNTRY_TUPLE_LIST = (
                self._includes_keywords_load(csv_config, list_regex))

        return self._includes_keywords(
            csv_config, RegexDetection.SCOPE_COUNTRY_TUPLE_LIST, query_func)

    SCOPE_SUBDIVISION_TUPLE_LIST = None

    def _scope_subdivision(self) -> [Geographic]:
        csv_config = CsvInsert.config_subdivisions
        query_func = lambda row: (and_(Geographic.country == row['iso2'],
                                       (Geographic.subdivision
                                        == row['subdivision']),
                                       Geographic.location == None))
        regex_func1 = lambda row: ("(^.*\W+|^)" + row['name'] +
                                   "($|\W+.*$)")
        regex_func2 = lambda row: ("(^.*\W+|^)" + row['subdivision'] +
                                   "($|\W+.*$)")
        list_regex = [(regex_func1, re.IGNORECASE | re.MULTILINE),
                      (regex_func2, re.MULTILINE), ]

        if RegexDetection.SCOPE_SUBDIVISION_TUPLE_LIST is None:
            RegexDetection.SCOPE_SUBDIVISION_TUPLE_LIST = (
                self._includes_keywords_load(csv_config, list_regex))

        return self._includes_keywords(
            csv_config, RegexDetection.SCOPE_SUBDIVISION_TUPLE_LIST,
            query_func)

    SCOPE_LOCATION_TUPLE_LIST = None

    def _scope_location(self) -> [Geographic]:
        csv_config = CsvInsert.config_locations
        query_func = lambda row: (and_(Geographic.country == row['country'],
                                       Geographic.location == row['code'],
                                       Geographic.district == None))
        regex_func1 = lambda row: ("(^.*\W+|^)" + row['code'] +
                                   "($|\W+.*$)")
        regex_func2 = lambda row: ("(^.*\W+|^)(" + row['name'].
                                   replace('(', '\(').
                                   replace(')', '\)') + '|' +
                                   row['namewodiacritics'].
                                   replace('(', '\(').replace(')', '\)') +
                                   ")($|\W+.*$)")

        list_regex = [(regex_func1, re.MULTILINE),
                      (regex_func2, re.IGNORECASE | re.MULTILINE)]

        if RegexDetection.SCOPE_LOCATION_TUPLE_LIST is None:
            RegexDetection.SCOPE_LOCATION_TUPLE_LIST = (
                self._includes_keywords_load(csv_config, list_regex))

        return self._includes_keywords(
            csv_config, RegexDetection.SCOPE_LOCATION_TUPLE_LIST, query_func)

    IS_NUMBER_REGEX = re.compile("^[0-9]+$", re.MULTILINE |
                                 re.IGNORECASE)

    def scope_geographic(self) -> [Geographic]:
        l_continent = self._scope_continent()
        l_country = self._scope_country()
        l_subdivision = self._scope_subdivision()
        l_location = self._scope_location()

        s_cou_sub_loc = set()
        s_sub_loc = set()
        s_cou_loc = set()
        s_cou_sub = set()

        if l_country and l_subdivision and l_location:
            for (cou, sub, loc) in itertools.product(l_country, l_subdivision,
                                                     l_location):

                if (cou.country == sub.country and
                        sub.country == loc.country and
                        sub.subdivision == loc.subdivision):
                    s_cou_sub_loc.add(loc)

                if (sub.country == loc.country and
                        sub.subdivision == loc.subdivision):
                    s_sub_loc.add(loc)

                if cou.country == loc.country:
                    s_cou_loc.add(loc)

                if cou.country == sub.country:
                    s_cou_sub.add(sub)

        else:
            for (sub, loc) in itertools.product(l_subdivision, l_location):
                if (sub.country == loc.country and
                        sub.subdivision == loc.subdivision):
                    s_sub_loc.add(loc)

            for (cou, loc) in itertools.product(l_country, l_location):
                if cou.country == loc.country:
                    s_cou_loc.add(loc)

            for (cou, sub) in itertools.product(l_country, l_subdivision):
                if cou.country == sub.country:
                    s_cou_sub.add(sub)

        if s_cou_sub_loc:
            return list(s_cou_sub_loc)
        if s_cou_loc:
            return list(s_cou_loc)
        if s_sub_loc:
            return list(s_sub_loc)
        if s_cou_sub:
            return list(s_cou_sub)
        if l_location:
            l_location_new = []

            # There are a cities with name "Peer" or Peerie -> remove it
            # (numbers also), since it is a network keyword
            # Also MED is keyword and will be excluded
            for loc in l_location:

                m_regex = RegexDetection.IS_NUMBER_REGEX.match(
                    str(loc.subdivision))  # str() converts
                # also None
                if not (m_regex or
                        (loc.country == "BE" and loc.location == "PEE") or
                        (loc.country == "CA" and loc.location == "PEE") or
                        (loc.country == "EE" and loc.location == "PRI") or
                        loc.location == "MED"):
                    l_location_new.append(loc)

            if l_location_new:
                return l_location_new

        if l_subdivision:
            l_subdivision_new = []

            # Check if the subdivision is a number, then not return it
            for sub in l_subdivision:

                m_regex = RegexDetection.IS_NUMBER_REGEX.match(
                    str(sub.subdivision))  # str() converts
                # also None
                if not m_regex:
                    l_subdivision_new.append(sub)

            if l_subdivision_new:
                return l_subdivision_new

        if l_country:
            return l_country
        if l_continent:
            return l_continent

        return []

    @typechecked
    def scope(self):  # -> [Scope]:
        """Try to find one or more scopes in the description."""
        l_scopes = []

        l_scopes += self.scope_as()
        l_scopes += self.scope_peertype()
        l_scopes += self.scope_ixp()
        l_scopes += self.scope_geographic()

        return l_scopes

    DO_NOT_ANNOUNCE_REGEX = re.compile(
        ".*(not\s+announc|annouc\w*\s+not|"
        "Surpress\s+Announc|not\s+advertis|advertis\w+\s+not).*",
        re.IGNORECASE)

    DO_ANNOUNCE_REGEX = re.compile(".*(announc|advertis).*", re.IGNORECASE)

    def announcement(self, std_comm: optional(StdComm)=None
                     ) -> [StdComm.Announcement]:
        m_regex_not = RegexDetection.DO_NOT_ANNOUNCE_REGEX.match(
            self.description)

        m_regex = RegexDetection.DO_ANNOUNCE_REGEX.match(self.description)

        if m_regex_not:
            skip = True
        else:
            skip = False

        annos = []

        if m_regex or m_regex_not:
            scopes = self.scope()

            if not scopes:
                scopes = [None]

            for scope in scopes:
                anno = StdComm.Announcement(skip=skip, blackhole=False,
                                            scope=scope, auto_detected=True,
                                            std_comm=std_comm,
                                            auto_detected_scope=True)

                self.session.add(anno)
                annos.append(anno)

        return annos

    BLACKHOLE_REGEX = re.compile(".*blackhol.*", re.IGNORECASE)

    def blackhole(self, std_comm: optional(StdComm)=None
                  ) -> [StdComm.Announcement]:

        m_regex = RegexDetection.BLACKHOLE_REGEX.match(self.description)

        blacks = []

        if m_regex:
            scopes = self.scope()

            if not scopes:
                scopes = [None]

            for scope in scopes:
                black = StdComm.Announcement(skip=True, blackhole=True,
                                             scope=scope, auto_detected=True,
                                             std_comm=std_comm,
                                             auto_detected_scope=True)

                self.session.add(black)
                blacks.append(black)

        return blacks

    PREPEND_REGEX = re.compile(".*prepend.*", re.IGNORECASE)

    def prepend(self, std_comm: optional(StdComm)=None) -> [StdComm.Prepend]:

        m_regex = RegexDetection.PREPEND_REGEX.match(self.description)

        preps = []

        if m_regex:
            scopes = self.scope()

            times = self.prepend_times()

            if not scopes:
                scopes = [None]

            for scope in scopes:
                prep = StdComm.Prepend(times=times, std_comm=std_comm,
                                       scope=scope, auto_detected=True,
                                       auto_detected_scope=True)

                self.session.add(prep)
                preps.append(prep)

        return preps

    LOCALPREF_REGEX = re.compile(".*(localpref|local[ _-]pref|preferenc).*",
                                 re.IGNORECASE)

    def localpref(self, std_comm: optional(StdComm)=None
                  ) -> [StdComm.LocalPref]:

        m_regex = RegexDetection.LOCALPREF_REGEX.match(self.description)

        locs = []

        if m_regex:
            scopes = self.scope()

            pref = self.localpref_preference()

            if not scopes:
                scopes = [None]

            for scope in scopes:
                loc = StdComm.LocalPref(preference=pref, std_comm=std_comm,
                                        scope=scope, auto_detected=True,
                                        auto_detected_scope=True)

                self.session.add(loc)
                locs.append(loc)

        return locs

    def tagging(self, std_comm: optional(StdComm)=None) -> [StdComm.Tagging]:

        tags = []

        scopes = self.scope()

        if not scopes:
            scopes = [None]

        for scope in scopes:
            tag = StdComm.Tagging(std_comm=std_comm, scope=scope,
                                  auto_detected=True, auto_detected_scope=True)

            self.session.add(tag)
            tags.append(tag)

        return tags

    INCLUDE_REGEX = re.compile('\d+:\d+', re.MULTILINE | re.IGNORECASE)

    def include(self, std_comm: optional(StdComm)=None) -> [StdComm.Include]:

        values = RegexDetection.INCLUDE_REGEX.findall(self.description)
        incs = []

        if values:
            scopes = self.scope()

            if not scopes:
                scopes = [None]

            for (scope, value) in itertools.product(scopes, values):
                inc = StdComm.Include(std_comm=std_comm, scope=scope,
                                      auto_detected=True,
                                      auto_detected_scope=True)

                self.session.add(inc)
                incs.append(inc)

        return incs

    def include_set_includings(self, incs: [StdComm.Include]):
        pass  # TODO

    def taxonomy(self, std_comm: optional(StdComm)=None) -> [StdComm.Taxonomy]:
        taxos = []

        taxos += self.announcement(std_comm)
        taxos += self.blackhole(std_comm)
        taxos += self.prepend(std_comm)
        taxos += self.localpref(std_comm)

        if not taxos:
            taxos += self.tagging(std_comm)

        return taxos
