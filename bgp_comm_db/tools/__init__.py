__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"

from .regex_detection import *
from .csv_insert import *
from .statistics import *
