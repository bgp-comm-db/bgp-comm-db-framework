"""
Inserts into the db from various csv files.
"""

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['CsvInsert']

from bgp_comm_db.tools.country_continent import COUNTRY_CONTINENT
from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.progressbar import Bar
from bgp_comm_db.progressbar import FormatLabel
from bgp_comm_db.progressbar import Percentage
from bgp_comm_db.progressbar import ProgressBar
from datetime import datetime as DateTime
import csv
import dateutil.parser
import os
import re


class CsvInsert(object):

    """docstring for CsvInsert"""

    def __init__(self, csv_file: str, classname: type, comment_func,
                 maxval: int, fields: tuple, reader_args: dict,
                 options: dict=dict(), skip: list=list(), filter: list=list(),
                 comment_func_dict=None):
        super(CsvInsert, self).__init__()
        self.csv_file = os.path.realpath(csv_file)
        self.classname = classname
        self.comment_func = comment_func
        self.maxval = maxval
        self.fields = fields
        self.map = map
        self.reader_args = reader_args
        self.options = options
        self.skip = skip
        self.filter = filter
        self.session = None
        del comment_func_dict

    def __enter__(self):
        self.session = Session()

        # connect to the database
        # self.connection = engine.connect()

        # begin a non-ORM transaction
        # self.trans = self.connection.begin()

        # bind an individual Session to the connection
        # self.session = Session(bind=self.connection)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.session.rollback()
        else:
            widgets = [FormatLabel('Committing ...')]
            pbar = ProgressBar(widgets=widgets).start()
            self.session.commit()

            # rollback - everything that happened with the
            # Session above (including calls to commit())
            # is rolled back.
            # self.session.close()
            # self.trans.rollback()
            # self.connection.close()
            # Session.remove()

            self.session.close()
            pbar.clear_ln('Committing ... Finished')
        return

    def config_as_nicename(self, string: str):
        string_lst = string.rsplit(',', 1)

        if len(string_lst) != 2:
            raise Exception("Could not extract Country from string: ",
                            string_lst, " --- ", string)

        geo = self.session.query(Geographic).filter(
            Geographic.country == string_lst[1]).filter(
            Geographic.subdivision == None).filter(
            Geographic.location == None).filter(
            Geographic.district == None).first()

        if geo == None:
            raise Exception(str(string) + " Country not found.")

        return {'nicename': string_lst[0], 'geographic_id': geo.sgk}

    @staticmethod
    def inserts():

        dummy_scope = Scope()
        dummy_std_comm = StdComm()
        session = Session()
        session.add(dummy_scope)
        session.add(dummy_std_comm)
        session.commit()
        session.close()

        with CsvInsert(**CsvInsert.config_continets) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_countries) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_subdivisions) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_locations) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_as_reserved_private) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_as_dummy) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_as_normal) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_peertypes) as csvinsert:
            csvinsert.insert()
        with CsvInsert(**CsvInsert.config_well_known) as csvinsert:
            csvinsert.insert()
        # IXPs after locations!
        with CsvInsert(**CsvInsert.config_ixps) as csvinsert:
            csvinsert.insert()

    config_as_reserved_private = {
        'classname': Ases,
        'fields': (lambda self, s: {'asn': s[2:].strip()},  # `2:` remove "AS"
                   lambda self, s: {
                       'name': s, 'datetime': DateTime(1970, 1, 1)},
                   config_as_nicename),
        'reader_args': {'delimiter': ';', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'maxval': 72324,
        'skip': [],
        'filter': [
            {'irow': 1, 'srow': 'name', 'regex': re.compile(
                'RESERVED|PRIVATE|PRIVATE-AS', re.MULTILINE | re.IGNORECASE)}
        ],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/ases/autnums-cut-new-4.txt"),
    }

    config_as_dummy = {
        'classname': Ases,
        'fields': (lambda self, s: {'asn': s[2:].strip()},  # `2:` remove "AS"
                   lambda self, s: {'name': "DUMMY-" + s,
                                    'datetime': DateTime(1970, 1, 1)},
                   config_as_nicename),
        'reader_args': {'delimiter': ';', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'maxval': 72324,
        'skip': [
            {'irow': 1, 'srow': 'name', 'regex': re.compile(
                'RESERVED|PRIVATE|PRIVATE-AS', re.MULTILINE | re.IGNORECASE)}
        ],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/ases/autnums-cut-new-4.txt"),
    }

    config_as_normal = {
        'classname': Ases,
        'fields': (lambda self, s: {'asn': s[2:].strip()},  # `2:` remove "AS"
                   lambda self, s: {'name': s,
                                    'datetime': DateTime(2015, 1, 14)},
                   config_as_nicename),
        'reader_args': {'delimiter': ';', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'maxval': 72324,
        'skip': [
            {'irow': 1, 'srow': 'name', 'regex': re.compile(
                'RESERVED|PRIVATE|PRIVATE-AS', re.MULTILINE | re.IGNORECASE)}
        ],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/ases/autnums-cut-new-4.txt"),
    }

    config_continets = {
        'classname': Geographic,
        'fields': (lambda self, s: {'continent': s},
                   lambda self, s: {},),
        'reader_args': {'quotechar': '"', 'delimiter': ';',
                        'skipinitialspace': True, },
        'comment_func': lambda row: row[0],
        'comment_func_dict': lambda row: row['short2'],
        'options': {'has_header': True,
                    'skip_blank_lines': True,
                    },
        'maxval': 8,
        'skip': [],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/continents.csv"),
    }

    config_countries = {
        'classname': Geographic,
        'fields': (lambda self, s: {'country': s},
                   lambda self, s: {'continent': s}),
        'reader_args': {'delimiter': ',', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'comment_func_dict': lambda row: row['country'],
        'maxval': 255,
        'skip': [],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/countries.csv"),
    }

    config_countries_iso3166 = {
        'classname': Geographic,
        'fields': (lambda self, s: {'country': s,
                                    'continent': COUNTRY_CONTINENT[s], },  # iso2
                   lambda self, s: {},  # name
                   lambda self, s: {},  # iso3
                   lambda self, s: {},),  # num3
        'reader_args': {'quotechar': '"', 'delimiter': ',',
                        'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'comment_func_dict': lambda row: row['iso2'],
        'maxval': 255,
        'options': {'has_header': True,
                    'skip_blank_lines': True, },
        'skip': [],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/iso3166-countries.csv"),
    }

    config_subdivisions = {
        'classname': Geographic,
        'fields': (lambda self, s: {'continent': COUNTRY_CONTINENT[s],  # "iso2"
                                    'country': s},
                   lambda self, s: {},  # "name"
                   lambda self, s: {'subdivision': s},  # "subdivision"
                   ),
        'reader_args': {'quotechar': '"', 'delimiter': ',',
                        'skipinitialspace': True},
        'comment_func': lambda row: row[0] + '-' + row[2],
        'comment_func_dict': lambda row: row['iso2'] + '-' + row['subdivision'],
        'maxval': 3338,
        'options': {'has_header': True,
                    'skip_blank_lines': True, },
        'skip': [],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/subdivisions.csv"),
    }

    config_locations = {
        'classname': Geographic,
        'fields': (lambda self, s: {},  # ch
                   lambda self, s: {'continent': COUNTRY_CONTINENT[s],
                                    'country': s},  # country
                   lambda self, s: {'location': s},  # code (location)
                   lambda self, s: {},  # name
                   lambda self, s: {},  # namewodiacritics
                   lambda self, s: {'subdivision': s},  # subdiv
                   lambda self, s: {},  # function
                   lambda self, s: {},  # status
                   lambda self, s: {},  # date
                   lambda self, s: {},  # iata
                   lambda self, s: {},  # coordinates
                   lambda self, s: {},  # remarks
                   ),
        'reader_args': {'quotechar': '"', 'delimiter': ',',
                        'skipinitialspace': True},
        'comment_func': lambda row: row[1] + '-' + row[2],
        'comment_func_dict': lambda row: row['country'] + '-' + row['code'],
        'maxval': 99006,
        'options': {'has_header': True,
                    'skip_blank_lines': True,
                    },
        'skip': [
            {'irow': 3, 'srow': 'name', 'regex': re.compile(
                '^\..*$', re.MULTILINE | re.IGNORECASE)},
            {'irow': 2, 'srow': 'code', 'regex': re.compile(
                '^\s*$', re.MULTILINE | re.IGNORECASE)}
        ],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/unlocode.csv"),
    }

    config_well_known = {
        'classname': StdCommRange,
        'fields': (lambda self, s: {'as_id': Ases.get_at_datetime(
            self.session, (int(s, 16) >> 16) & 0xffff,
            DateTime(1970, 1, 1)).sgk,
            'from_value':  StdValue(s)},
            lambda self, s: {'description': s},
            lambda self, s: {'reference': s, 'observed_only': False},
            lambda self, s: {'datetime': dateutil.parser.parse(s),
                             'source': StdSource.rfc}
        ),
        'reader_args': {'delimiter': ',', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'maxval': 17,
        'options': {'has_header': True,
                    'skip_blank_lines': True,
                    },
        'skip': [
            {'irow': 1, 'regex': re.compile(
                r'Reserved', re.IGNORECASE)},
            {'irow': 1, 'regex': re.compile(
                r'Unassigned', re.IGNORECASE)},
        ],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/bgp-well-known-communities.csv"),
    }

    config_peertypes = {
        'classname': Peertype,
        'fields': (lambda self, s: {'type': s},
                   lambda self, s: {'description': s}),
        'reader_args': {'delimiter': ';', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'maxval': 14,
        'options': {'has_header': True,
                    'skip_blank_lines': True,
                    },
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/peertypes.csv"),
    }

    config_ixps = {
        'classname': Ixp,
        'fields': (lambda self, s: {'name': s},
                   lambda self, s: {'nicename': s},
                   lambda self, s: {},  # locode
                   lambda self, s: {},  # country
                   ),
        'reader_args': {'delimiter': ';', 'skipinitialspace': True},
        'comment_func': lambda row: row[0],
        'comment_func_dict': lambda row: row['name'],
        'maxval': 212,
        'options': {'has_header': True,
                    'skip_blank_lines': True,
                    },
        'skip': [],
        'csv_file': (config.get('PATHS', 'data_dir') +
                     "/ixps.csv"),
    }

    def _skip_row(self, i, row) -> bool:
        if self.options.get('has_header', False) and i == 0:
            return True
        if (self.options.get('skip_blank_lines', False) and
                not any(row)):
            return True
        for entry in self.skip:
            if bool(re.match(entry['regex'], row[entry['irow']])):
                return True
        for entry in self.filter:
            if not bool(re.match(entry['regex'], row[entry['irow']])):
                return True
        return False

    @typechecked
    def insert(self) -> type(None):
        """
        Insert something from a csv list
        """

        widgets = [FormatLabel('Inserting %(comment)s: '), Percentage(), Bar()]
        pbar = ProgressBar(widgets=widgets, maxval=self.maxval).start()

        with open(self.csv_file, mode='r', closefd=True) as file:

            reader = csv.reader(file, **self.reader_args)

            i = 0
            for i, row in enumerate(reader):

                if self._skip_row(i, row):
                    continue

                pbar.update(i + 1, comment=self.comment_func(row))

                args = dict()
                for j, func in enumerate(self.fields):
                    args.update(func(self, row[j]))

                if issubclass(self.classname, Ixp):
                    geo = self.session.query(Geographic).filter(
                        Geographic.country == row[3],
                        Geographic.location == row[2],
                        Geographic.district == None).first()
                    args.update({'geographic': geo})

                obj = self.classname(**args)
                self.session.add(obj)


                # FIXME For speed improvements the commit is run only every 10
                # inserts, but while inserting StdCommRange (Well Known
                # communities) only the last object which was added to session
                # is inserted.
                #if (i % 10) == 0:
                self.session.commit()

            pbar.clear_ln(
                str(i) + " " + str(self.classname) + " Objects inserted")
