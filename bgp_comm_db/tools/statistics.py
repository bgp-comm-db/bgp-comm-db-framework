
from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from bgp_comm_db.receipes import *
from sqlalchemy import func
from sqlalchemy.orm import aliased
from sqlalchemy.orm import noload
from sqlalchemy.orm import subqueryload
from sqlalchemy.orm import eagerload
from sqlalchemy.orm import lazyload
from sqlalchemy.orm import joinedload

import time

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['Statistics']


class Statistics(ConnectionSession):

    def __init__(self, rollback_trans: bool=True):
        super(Statistics, self).__init__(rollback_trans)
        self.connection = None
        self.trans = None
        self.session = None

        self.start = None

    def __exit__(self, exc_type, exc_val, exc_tb):

        if self.rollback_trans:
            self.session.commit()

            # rollback - everything that happened with the
            # Session above (including calls to commit())
            # is rolled back.
            self.session.close()
            self.trans.rollback()
            self.connection.close()  # return connection to the Engine

        else:
            if exc_type:
                self.session.rollback()
            else:
                self.session.commit()
                self.session.close()

        end = time.time()
        print(end - self.start)

    # @typechecked
    def count_comm(self):

        self.start = time.time()

        amount_dict = dict()
        for src in StdSource:
            amount_dict[src] = 0

        a_range1 = aliased(StdCommRange)

        amount_single = (self.session.
                         query(a_range1.source, func.count(a_range1.source)).
                         filter(a_range1.to_value == None).
                         with_transformation(
                             StdCommRange.latest_entry(a_range1)).
                         group_by(a_range1.source).
                         all())

        for source, count in amount_single:
            amount_dict[source] = count

        print("Singles:", amount_dict)

        amount_multi = (self.session.
                        query(a_range1).
                        filter(a_range1.to_value != None).
                        with_transformation(
                            StdCommRange.latest_entry(a_range1))
                        )

        # 50 also seem to works
        for range_ in yield_limit(amount_multi, a_range1.sgk, 40):
            print(range_)
            a_field = aliased(StdCommField)

            amount_in_field = (self.session.
                               query(a_field.group, func.count(a_field.group)).
                               filter(a_field.range_ == range_).
                               with_transformation(
                                   StdCommField.latest_entry(a_field)).
                               group_by(a_field.group).
                               all())

            mul = 1
            for _, count in amount_in_field:
                mul = mul * count

            amount_dict[range_.source] += mul
            print(amount_dict)

        print("Singles:", amount_dict)

        return amount_dict

    def count_comm_unique(self, defined_by_asn=None):

        self.start = time.time()

        a_range1 = aliased(StdCommRange)
        comm_set = set()

        q_range = (self.session.
                         query(a_range1).
                         with_transformation(
                             StdCommRange.latest_entry(a_range1)).
                         options(noload('*'), eagerload("fields"))
                         )

        if defined_by_asn is not None:
            q_range = q_range.filter(a_range1.asn == defined_by_asn)

        # 50 also seem to works
        for range_ in yield_limit(q_range, a_range1.sgk, 30):
            if range_.to_value is None:
                comm_set.add((range_.asn, range_.from_value.value))

            else:
                for field in range_.field_values():
                    value = field.value.value
                    comm_set.add((range_.asn, value))

            # debug(comm_set)


        return comm_set
