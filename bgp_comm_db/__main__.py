'''
Created on 15.02.2015

@author: raabf
'''

if __name__ == "__main__" and __package__ is None:
    __package__ = "bgp_comm_db"  # PEP 366

from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.receipes import *
from bgp_comm_db.thread_with_exc import ThreadWithExc
from bgp_comm_db.tools import *
from datetime import datetime as DateTime
from logging import critical
from logging import debug
from logging import error
from logging import info
from logging import warning
from marshmallow import pprint
from sqlalchemy import desc
from sqlalchemy.orm import aliased
from sqlalchemy.orm import eagerload
from sqlalchemy.orm import joinedload
from sqlalchemy.orm import lazyload
from sqlalchemy.orm import load_only
from sqlalchemy.orm import mapper
from sqlalchemy.orm import noload
from sqlalchemy.orm import subqueryload
from sqlalchemy.orm import with_polymorphic
from sqlalchemy.sql.expression import select
from sqlalchemy_utils.functions import *
from tabulate import tabulate
import click
import dateutil.parser
import inspect
import mysql
import os
import sqlalchemy as sa
import time

from sqlalchemy import func
from sqlalchemy import distinct

from sqlalchemy.sql.expression import BinaryExpression
from sqlalchemy.sql import bindparam

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"


import cProfile
import io
import pstats
import contextlib


@contextlib.contextmanager
def profiled():
    pr = cProfile.Profile()
    pr.enable()
    yield
    pr.disable()
    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
    ps.print_stats()
    # uncomment this to see who's calling what
    # ps.print_callers()
    print(s.getvalue())


@click.group()
def main():
    pass

insert_opt_rollback_trans = True


@main.group()
@click.option('--rollback-trans/--no-rollback-trans', default=True,
              help="Use a special transaction "
              "which will be rolled back at the end. "
              "In the DB nothing will be changed.")
def insert(rollback_trans):
    global insert_opt_rollback_trans
    insert_opt_rollback_trans = rollback_trans


class Playing(ConnectionSession):

    """docstring for Playing"""

    def __init__(self):
        super(Playing, self).__init__(rollback_trans=False)

    def playing(self):
        print("###### Fetch Community #######")
        # search for 6461:5060 which is valid now (StdCommRange.datetime <=
        # DateTime.now()) on an as (<= StdValue(6461, 5060)) which is valid now
        # (StdValue set by default now() as datetime))
        range1 = self.session.query(StdCommRange).with_transformation(
            StdCommRange.from_value.transformer).order_by(
            desc(StdCommRange.datetime)).filter(
                StdCommRange.from_value <= StdValue(12732, 10030),
            StdCommRange.datetime <= DateTime.now(),
            StdCommRange.to_value == None).first()

        print(range1)
        print(tabulate(range1.taxonomies_scopes_to_dict(), headers="keys"))

        print("###### Add Geo #######")

        # add an geo
        geo1 = Geographic(continent=0b1, country='SE', subdivision='05')

        self.session.add(geo1)
        self.session.commit()

        print(geo1)

        as1 = Ases(
            asn=7653,
            name='GOD-AS',
            datetime=DateTime.now(),
            geographic=geo1)

        self.session.add(as1)
        self.session.commit()

        print(as1, as1.geographic)

        print("###### New Community with fields #######")

        # Create a new Community with fields

        tum_i8_as = self.session.query(Ases).filter(
            Ases.name == "TUM-I8-AS").scalar()

        berlin = self.session.query(Geographic).filter(
            Geographic.geo_short == 'EU-DE-*-BER-None').scalar()

        print(
            tabulate(
                (tum_i8_as.to_dict(),
                 berlin.to_dict()),
                headers="keys"))

        range2 = StdCommRange(from_value=StdValue(56357, 20),
                              datetime=DateTime(2015, 5, 17),
                              ases=tum_i8_as,
                              to_value=StdValue(56357, 29),
                              description="Prepend n times to Berlin, DE",
                              reference="My Brain",
                              observed_only=False,
                              field_captures="times",
                              source=StdSource.unspecific)

        self.session.add(range2)
        self.session.commit()

        print(range2)

        field0 = StdCommField(
            datetime=DateTime(2015, 5, 17),
            offset=0,
            group=0,
            range_=range2,
            description="Do not announce to Berlin, DE")
        anno0 = StdComm.Announcement(
            skip=True,
            blackhole=False,
            std_comm=field0,
            scope=berlin,
            auto_detected=False,
            auto_detected_scope=False)
        self.session.add(field0)
        self.session.add(anno0)
        self.session.commit()
        print(field0)
        print(tabulate(field0.taxonomies_scopes_to_dict(), headers="keys"))

        field1 = StdCommField(
            datetime=DateTime(2015, 5, 17),
            offset=1,
            group=0,
            range_=range2,
            description="Prepend 1 times to Berlin, DE")
        prep1 = StdComm.Prepend(
            times=1,
            std_comm=field1,
            scope=berlin,
            auto_detected=False,
            auto_detected_scope=False)
        self.session.add(field1)
        self.session.add(prep1)
        self.session.commit()
        print(field1)
        print(tabulate(field1.taxonomies_scopes_to_dict(), headers="keys"))

        field2 = StdCommField(
            datetime=DateTime(2015, 5, 17),
            offset=2,
            group=0,
            range_=range2,
            description="Prepend 2 times to Berlin, DE")
        prep2 = StdComm.Prepend(
            times=2,
            std_comm=field2,
            scope=berlin,
            auto_detected=False,
            auto_detected_scope=False)
        self.session.add(field2)
        self.session.add(prep2)
        self.session.commit()
        print(field2)
        print(tabulate(field2.taxonomies_scopes_to_dict(), headers="keys"))

        field9 = StdCommField(
            datetime=DateTime(2015, 5, 17),
            offset=9,
            group=0,
            range_=range2,
            description="Announce to Berlin, DE")

        anno9 = StdComm.Announcement(
            skip=False,
            blackhole=False,
            std_comm=field9,
            scope=berlin,
            auto_detected=False,
            auto_detected_scope=False)
        self.session.add(field9)
        self.session.add(anno9)
        self.session.commit()
        print(field9)
        print(tabulate(field9.taxonomies_scopes_to_dict(), headers="keys"))

        self.session.commit()

        # Try two single detection
        print("###### Detection #######")

        det1 = RegexDetection("Do not announce to global AOL (AS1668)",
                              range2.from_value, self.session)
        det2 = RegexDetection("Prepend 2 times to München, DE",
                              StdValue(56357, 22), self.session)

        # I choose simply randomly range2 and field2 to connect the founded
        # taxonomies with some StdComm. Tey are not related with the searched
        # Text in this case.
        taxonomies1 = det1.taxonomy(range2)
        taxonomies2 = det2.taxonomy(field2)
        self.session.commit()
        print(range2)
        print(tabulate(range2.taxonomies_scopes_to_dict(), headers="keys"))
        print(field2)
        print(tabulate(field2.taxonomies_scopes_to_dict(), headers="keys"))

    def marshal(self):
        range1 = StdComm.get_std_comm(self.session, StdValue(12732, 10030))
        print(range1)

        range1_schema = StdCommRange.Schema()
        result1 = range1_schema.dumps(range1)
        pprint(result1.data)

        # local_schema = StdComm.LocalPref.Schema()
        # result_local = local_schema.dumps(range1.taxonomies[0])
        # print(range1.taxonomies)
        # pprint(result_local.data)

        # ases_schema = Ases.Schema()
        # result2 = ases_schema.dumps(range1.ases)
        # pprint(result2.data)

    def get_comm(self):

        # self.session.query(StdComm.Prepend).all()

        value_ = StdValue("01299:05003")
        # value_ = StdValue("65003:24940")
        # cache = FixTimeStdValuesCache(self.session)
        # comm = cache.get_std_comm(value_, source=StdSource.manual)
        # print(el)
        comm = StdComm.get_std_comm(self.session, value_,
                                    source=StdSource.manual,
                                    options=eagerload("fields"),
                                    eager_taxonomies=True)
        # print(value_, comm[0].fields[0].description)
        print(comm)
        # for f in comm[0].range_.fields:
        #     if f.offset == 5000:
        #         print(f)
        # time.sleep(5)
        # print(comm[0].fields[0].prepends)

    def get_comm_fields(self):
        datetime = None
        std_value = StdValue("01299:05003")
        # std_value = StdValue("01299:05000")
        std_value = StdValue(1273, 31000)
        # std_value = StdValue("65000:08928")
        # std_value = StdValue("65003:04134")
        std_value = StdValue("65000:16509")
        asn = None
        source = None #StdSource.xml2007

        options=None
        # options = [load_only('sgk', 'from_val', 'from_asn',
        #                      '_to_val', 'to_asn')]

        field_value = StdComm.get_std_comm_eager_fields(
            self.session, std_value, source, options=options)

        print(field_value)

    def get_comm_fields_timer(self):
        # based on
        # http://stackoverflow.com/questions/9437498/sqlalchemy-stopping-a-long-running-query?rq=1

        import threading
        datetime = None
        std_value = StdValue("01299:05003")
        # std_value = StdValue("01299:05000")
        std_value = StdValue(1273, 31000)
        # std_value = StdValue("65000:08928")
        std_value = StdValue("65003:04134")
        std_value = StdValue("65000:16509")
        # std_value = StdValue("15412:04602")
        asn = None
        source = None #StdSource.xml2007

        # conn = self.session.connection()

        options=None

        def comm_exec():

            field_value = StdComm.get_std_comm_eager_fields(
                self.session, std_value, source, options=options)
            print(field_value)

        thread = ThreadWithExc(target=comm_exec)

        def timer_cancel():
            print("It takes too long cancelling...")
            # print(self.connection.in_transaction())
            # print(self.connection.__dict__)
            # self.session.close()
            # # self.trans.close()
            # self.connection.close()
            # engine.dispose()

        # timer = threading.Timer(8, timer_cancel)

        thread.start()
        # timer.start()

        # options = [load_only('sgk', 'from_val', 'from_asn',
        #                      '_to_val', 'to_asn')]
        thread.join(timeout=8)

        if thread.is_alive():
            print("It takes too long, cancelling ...")
            while thread.is_alive():
                print(".")
                thread.raiseExc(TypeError)
                time.sleep(0.1)
        # if timer.is_alive():
        #     timer.cancel()

        # print(field_value)
        print("fin")

    def select_execute(self):
        sel = select([StdCommRange]).filter(StdCommRange.sgk == 23458)

        res = self.session.execute(sel)
        print(res)

    def all_anno(self):
        result = self.session.query(StdComm.Announcement).all()

        for i, res in enumerate(result):
            print(i, res)

            if i > 100:
                break

    def many_get_comm(self):

        number_results = 0

        tier1 = [7018, 174, 3320, 3491, 286, 209, 2561, 3356, 2914, 5511, 1239,
                 6453, 6762, 12956, 1299, 701, 2828, 6461, 15290, 5400, 1273]

        number_queries = len(tier1) * ((2**16)-1)

        start = time.time()

        for tier1_asn in tier1:

            for i in range(0, (2**16)-1):

                std_value = StdValue(tier1_asn, i)
                result = StdComm.get_std_comm(self.session, std_value)

                if result is not None:
                    print(result)
                    number_results += 1

        end = time.time()

        delta = end-start

        print("RESULTS ", number_results, "/", number_queries,
              " TIME: ", delta,
              " TIME/QUERY: ", delta.total_seconds()/number_queries)

    def group_iter(self):
        range_ = self.session.query(StdCommRange).filter(
            StdCommRange.sgk == 30337).one()

        print(range_)
        print(range_.group_numbers)

        field = StdCommField(1, 2, DateTime(2008, 1, 1), range_=range_,
                             description="Test")

        self.session.add(field)
        self.session.commit()

        assert(2 in range_.group_numbers)

        # for value in range_.field_values_iterator():
        #     print(value.value)

    def allranges(self):
        ranges = StdComm.get_all_ranges(
            self.session, 3491, DateTime(2008, 1, 1))
        print(tabulate((r.to_dict() for r in ranges), headers='keys'))

    def simple(self):
        tum_i8_as = self.session.query(Ases).filter(
            Ases.name == "TUM-I8-AS").scalar()

        range2 = StdCommRange(from_value=StdValue(56357, 20),
                              datetime=DateTime(2015, 5, 17),
                              as_id=226033,
                              to_value=StdValue(56357, 29),
                              description="Prepend n times to Berlin, DE",
                              reference="My Brain",
                              observed_only=False,
                              field_captures="times",
                              source=StdSource.unspecific)

        self.session.add(range2)
        self.session.commit()

        print(range2)

    def comm(self):
        datetime = None
        std_value = StdValue("01299:05003")
        # std_value = StdValue(3491, 60043)
        source = None
        session = self.session
        asn = None

        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        # TODO replace by StdCommRange.latest_range
        q_range = (session.query(StdCommRange).
                   filter(StdCommRange.datetime <= datetime).
                   order_by(desc(StdCommRange.datetime)).
                   with_transformation(StdCommRange.from_value.transformer).
                   filter(StdCommRange.from_value <= std_value.str_value).
                   with_transformation(StdCommRange.to_value.transformer).
                   filter(std_value.str_value <= StdCommRange.to_value)
                   )

        if source is not None:
            q_range = q_range.filter(StdCommRange.source == source)

        # TODO replace with all()
        range_ = q_range.first()

        if range_ is None or range_.to_value is None:
            # This range have no fields we have to fetch
            print(range_)
            return

        group_count = (session.query(StdCommField.group).
                       distinct(StdCommField.group).
                       filter(StdCommField.range_ == range_).
                       count()
                       )

        print(group_count)

        query_tables = []
        addition = range_.from_value.value

        for i in range(group_count):
            a_field1 = aliased(StdCommField)
            a_field2 = aliased(StdCommField)

            subquery = (session.query(a_field1).
                        outerjoin(a_field2,
                                  (a_field1.range_id == a_field2.range_id) &
                                  (a_field1.offset == a_field2.offset) &
                                  (a_field1.group == a_field2.group) &
                                  (a_field1.datetime < a_field2.datetime)
                                  ).
                        filter(a_field1.range_ == range_,
                               a_field1.datetime <= datetime
                               ).
                        filter(a_field2.range_id == None,
                               a_field2.offset == None,
                               a_field2.group == None
                               ).
                        subquery()
                        )
            # subquery = session.query(a_field1).filter(
            #     a_field1.range_ == range_,
            #     a_field1.datetime <= datetime
            # ).subquery()

            query_tables.append(aliased(a_field1, subquery))

        query = session.query(*query_tables)

        for i, subq in enumerate(query_tables):
            # addition = addition + subq.c.std_field_offset
            addition = addition + subq.offset
            print(subq)
            # query = query.filter(subq.range_ == range_)
            # query = query.select_entity_from(subq)

            if i >= 1:
                # query = query.filter(query_tables[i-1].c.std_field_group
                #                      < subq.c.std_field_group)
                query = query.filter(query_tables[i-1].group < subq.group)

        print(addition)
        query = query.filter(addition == std_value.value)

        print(query)

        result = query.first()

        print("Result:", result)

        if result is None:
            return None

        print(result)

        field_value = StdCommFieldValue(range_, result)

        print(field_value)

    def func(self):
        def __flat(*args):
            lst = []

            for a in args:
                if isinstance(a, (tuple, list)):
                    lst += __flat(*a)
                else:
                    lst.append(a)
            return lst

        print(__flat(2, (6, 7, (4, 5, 6), (4, 6), 7), 8))
        print(__flat(1, (2, 3), 4))

    def get_range(self):
        datetime = None
        std_value = StdValue("01299:05003")
        # std_value = StdValue("01299:05000")
        # std_value = StdValue(1273, 31000)
        session = self.session
        asn = None
        source = StdSource.manual

        # _get_std_comm_op(session, std_value: StdValue, op,
        #                  source: optional(StdSource)=None,
        #                  asn: optional(int)=None,
        #                  datetime: optional(DateTime)=None):
        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        q_range = (session.query(StdCommRange).
                   filter(StdCommRange.datetime <= datetime).
                   filter(StdCommRange.from_value <= std_value.str_value).
                   filter(StdCommRange.to_value >= std_value.str_value).
                   # filter(StdCommRange.from_value <= std_value).
                   # filter(StdCommRange.to_value == None).
                   options(eagerload(StdCommRange.fields)).
                   options(lazyload(StdCommRange.ases)).
                   options(eagerload(StdCommRange.announcements), eagerload(StdCommRange.localprefs), eagerload(StdCommRange.taggings), eagerload(StdCommRange.prepends)).
                   with_transformation(
            StdCommRange.latest_entry(StdCommRange)
        )
        )

        if source is not None:
            q_range = q_range.filter(StdCommRange.source == source)

        # theoretically if sources set, only scalar() should hold. But the DB
        # does not guarantee that, so should we raise an exception?

        # if source is not None:
        #     range_lst = q_range.scalar()
        # else:
        range_lst = q_range.all()
        print(range_lst)
        print(range_lst[0].taxonomies)
        print(range_lst[0].field_value(std_value))

    def get_range2(self):
        range1 = (self.session.
                  query(StdCommRange).
                  filter(
                      StdCommRange.from_value <= StdValue(12732, 10030),
                      StdCommRange.datetime <= DateTime.now(),
                      StdCommRange.to_value == None
                  ).
                  with_transformation(StdCommRange.latest_entry(StdCommRange)).
                  first()
                  )

        print(range1)

    def get_comm_cache(self):

        value_ = StdValue("01299:05003")
        # value_ = StdValue(56357, 65)

        cache = FixTimeStdValuesCache(self.session, value_.datetime)

        field_value = cache.get_std_comm(value_)

        print(field_value)

        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))
        print(cache.get_std_comm(value_))

    def eager(self):
        value_ = StdValue("01299:05000")

        a_range1 = aliased(StdCommRange)
        q_range = (self.session.
                   query(a_range1).
                   with_transformation(
                       StdCommRange.latest_entry(a_range1)).
                   filter(a_range1.from_value <= value_).
                   options(noload('*'), eagerload("fields"))
                   ).first()

        print(q_range)

        for f in q_range.field_values():
            print(f.value)

    def custq(self):
        value_ = StdValue("01299:05000")

        a_range1 = aliased(StdCommRange)
        a_range2 = aliased(StdCommRange)
        # q_range = (self.session.
        #            query(a_range1).
        #            outerjoin(a_range2,
        #                      (a_range1.as_id == a_range2.as_id)
        #                      ).
        #            filter(a_range1.description == "HELLO WORLD")
        #            )

        t_range = StdCommRange.__table__
        w_range = with_polymorphic(StdCommRange, [])
        p_range = mapper(
            StdCommRange,
            StdCommRange.__table__,
            non_primary=True)
        q_range = (self.session.
                   query(p_range)
                   # with_polymorphic(StdCommRange).
                   # filter(p_range.c.description == "HELLO WORLD")
                   )
        print(q_range)

    def check(self):
        value_ = StdValue("01299:05000")

        a_range1 = aliased(StdCommRange)
        q_range = (self.session.
                   query(a_range1).
                   with_transformation(
                       StdCommRange.latest_entry(a_range1)).
                   filter(a_range1.from_value <= value_).
                   options(noload('*'), eagerload("fields"))
                   ).first()

    def tax_all_load(self):

        tmp = self.session.query(StdComm.LocalPref.std_comm_id).all()
        (STATISTICS3_LOCALPREF_SET,) = map(set, zip(*tmp))
        print(len(STATISTICS3_LOCALPREF_SET))
        # time.sleep(2)

        tmp = self.session.query(StdComm.Announcement.std_comm_id).all()
        (STATISTICS3_ANNOUNCEMENT_SET,) = map(set, zip(*tmp))
        print(len(STATISTICS3_ANNOUNCEMENT_SET))
        tmp = self.session.query(StdComm.Prepend.std_comm_id).all()
        (STATISTICS3_PREPEND_SET,) = map(set, zip(*tmp))
        print(len(STATISTICS3_PREPEND_SET))
        # time.sleep(2)

        tmp = self.session.query(StdComm.Tagging.std_comm_id).all()
        (STATISTICS3_TAGGING_SET,) = map(set, zip(*tmp))
        print(len(STATISTICS3_TAGGING_SET))

        # STATISTICS3_TAGGING_SET = set()
        # query = self.session.query(StdComm.Tagging).options(load_only('sgk', 'std_comm_id'))
        # for ele in yield_limit(query, StdComm.Tagging.sgk, maxrq=300):
        # tmp = self.session.query(StdComm.Tagging.std_comm_id).all()
        # (STATISTICS3_TAGGING_SET,) = map(set, zip(*tmp))
        #     STATISTICS3_TAGGING_SET.add(ele.std_comm_id)
        # print(len(STATISTICS3_TAGGING_SET))


@main.command()
@click.argument('function')
def do(function):
    try:
        with Playing() as playing_:
            func = getattr(playing_, function)
            func()
    except sa.exc.InterfaceError as ie:
       print("INTERFACE ERROR CATCHED sa")
    except mysql.connector.errors.InterfaceError as ie:
        print("INTERFACE ERROR CATCHED mysql")


@main.command()
def process():
    # connect to the database
    connection = engine.connect()

    # begin a non-ORM transaction
    trans = connection.begin()

    # bind an individual Session to the connection
    session = Session(bind=connection)

    as_ = Ases(asn=666666, name="test", datetime=DateTime.now())

    range2 = StdCommRange(from_value=StdValue(56357, 20),
                          datetime=DateTime(2015, 5, 17),
                          # as_id=226033,
                          ases=as_,
                          to_value=StdValue(56357, 29),
                          description="Prepend n times to Berlin, DE",
                          reference="My Brain",
                          observed_only=False,
                          field_captures="times",
                          source=StdSource.unspecific)

    session.add(as_)
    session.add(as_)
    session.commit()

    session.close()
    trans.rollback()
    connection.close()

    # session = Session()

    # as1 = Ases(asn=56, name="Sensei")

    # as2 = session.query(Ases).first()

    # comm1 = StdCommRange(ases=as2, from_val=67, observed_only=True,
    #     description="Hunted")

    # session.add(as1)
    # session.add(comm1)

    # comm2 = session.query(StdComm).first()

    # print(comm2)

    # pref1 = StdCommRangeLocalPref(std_comm=comm2,
    #     preference=300)

    # anno1 = StdComm.Announcement.Range(std_comm=comm2, scope=as2, skip=False)

    # session.add(anno1)

    # session.commit()

    # print(str(anno1) + str(anno1.std_comm))
    # print(comm2.announcements)

    # session.add(pref1)

    # session.commit()

    # print(pref1)

    # print("Anno:" + str(anno1))

    # Geographic

    # geo1 = Geographic(continent=0b1, country='SE', subdivision='05')
    # print(geo1)

    # session = Session()
    # session.add(geo1)
    # geo_de = session.query(Geographic).filter(
    #     Geographic.continent=="EU",
    #     Geographic.country=="CZ",
    #     Geographic.subdivision == None).first()

    # print(geo_de)
    # print(StdValue(65535, 4))
    # range = session.query(StdCommRange).filter(
    #     StdCommRange.from_value == "65535:04",
    #     StdCommRange.to_value == None).first()

    # anno = StdComm.Announcement(skip=True,
    #                             std_comm=range,
    #                             scope_id=5,
    #                             blackhole=False,
    #                             auto_detected=False,
    #                             auto_detected_scope=True)
    # print("\t\t", anno._scope_id)
    # print(pref_none, pref_none.scope)
    # pref_none.scope = None
    # pref_none.scope_id = 1
    # print(range)
    # print(pref_none.scope)
    # print(inspect(pref_none.scope.__class__).primary_key[0].name)
    # pref_none.scope = None
    # print(pref_none, pref_none.scope)

    # accept_own = StdValue(65535, 1)
    # range = session.query(StdCommRange).filter(
    #     StdCommRange.from_value == accept_own).first()
    # session.add(pref_none)

    # session.commit()

    # det = RegexDetection("Bla ML Paris AS3311 DE-CIX", StdValue("324:23"), session)
    # print(det.scope())

    # session.commit()

    # funct = lambda query:
    # query.order_by(func.abs(9-cast(StdCommRange._from_asn,
    # BIGINT(unsigned=False))))

    # min = funct(session.query(StdCommRange)).first()

    # range_ = session.query(StdCommRange).join(StdComm.LocalPref).with_transformation(
    #     StdCommRange.from_value.transformer).filter(
    #     StdCommRange.from_value <= StdValue(65535,1)).all()

    # for key in range_.__mapper__.c.keys():
    #     if not key.startswith('_'):
    #         dict_[key] = getattr(range_, key)

    # print(tabulate([range_.to_dict()], headers="keys")) x.to_dict for x in
    # range_

    # print(tabulate((x.to_dict() for x in range_), headers="keys"))

    # from sqlalchemy import inspect
    # from sqlalchemy.ext.hybrid import hybrid_property

    # for key, prop in inspect(range_.__class__).all_orm_descriptors.items():
    #     if isinstance(prop, hybrid_property):
    #         dict_[key] = getattr(range_, key)

    # print(dict_)

    # from sqlalchemy.orm import class_mapper

    # for col in class_mapper(range_.__class__).mapped_table.c:
    #     print(col)

    # print(range_.__dict__)

    # range.from_value = StdValue(4,7)

    # session.add(range)
    # session.commit()

    # print(range)
    # for r in range:
    #     print(r)
    # print(range.to_value)

    # det = RegexDetection("Do not announce to AOL (AS1668)",
    #  StdValue(6461,5530), session)
    # geos = det.scope_geographic()
    # print(geos)

    # Update datetime in fields

    # for i, field in enumerate(session.query(StdCommField).all()):
    #     field.datetime = field.range_.datetime
    #     session.add(field)

    #     if (i % 30) == 0:
    #         print("reach", i)
    #         # session.commit()

    # session.rollback()
    # session.close()


@main.command()
def count_comm():
    with Statistics() as statistics:
        count = statistics.count_comm()
        print(count)


@main.command()
@click.option('--asn', type=int, default=None)
def count_comm_unique(asn):
    print("defined by asn:", asn)
    with Statistics() as statistics:
        comm_set = statistics.count_comm_unique(asn)
        print(len(comm_set))


if __name__ == '__main__':
    main()
