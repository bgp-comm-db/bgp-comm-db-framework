
# from tabulate import tabulate


def surround(iterable, *args, **kwargs):
    """Iterate with a tuple and multiple defaulted previous and next elements.

    You can define here previous and next elements which can leave your bounds
    of your `iterable` or not. surround() splits a sequence of elements in three
    parts *prev*, *curr* and *next*. *prev* and *next* can leave the bounds of
    `iterable` and return with the value `default` if they are out of bound.
    *curr* is an tuple which is always in bound and can also contain multiple
    elements.

    Args:
        iterable:
            An iterator or iterable object from which the sequence
            should be produced.
        *args:
            Defines the value of k, l and m. See *args parameter* section.
        default (keyword only) = None:
            The default value p_1, p_2, ..., p_k and n_1, n_2, ..., n_m will
            get if they are actually out of bound of `iterable`.
        pack_tuple (bool, keyword only) = True:
            A boolean value which indicates if the p, c and n sequences should
            be returned respectively in a tuple. See *Returns* section.

    args parameter:
        `*args` can be either one the following:

        l:
            l must be an int.
            k = 0
            l = l
            m = 0

        k, l, m:
            All must be ints.
            k = k
            l = l
            m = m

        prev, [curr], next:
            Where `prev`, `curr` and `next` can be either
             (with e, t in {(p, k), (c, l), (n, m)}):

            e_1, e_2, ..., e_t:
                A sequence of variables or values. They can have any type
                except int. The values of the variables are ignored only the
                amount is counted.
                t = "amount of arguments"
            "e_1, e_2, ..., e_t":
                A string, which represent a sequence. Only the ',' are counted.
                t = "number of ',' in the string" + 1
            t:
                An int.
                t = t

        "p_1, p_2, ..., p_k, [c_1, c_2, ..., c_l], n_1, n_2, ..., n_m":
            An string, which represent three sequences. Also here only the ','
            are respectively counted.

    Returns:
        if pack_tuple == True (default):
        (p_1, p_2, ..., p_k), (c_1, c_2, ..., c_l), (n_1, n_2, ..., n_m)

        (p_1, p_2, ..., p_k), (c_1, c_2, ..., c_l) or (n_1, n_2, ..., n_m) are
        respectively not tuples if respectively k == 1, l == 1 or m == 1 holds
        and are omitted if if respectively k == 0, l == 0 or m == 0 holds.

        if pack_tuple == False:
        p_1, p_2, ..., p_k, c_1, c_2, ..., c_l, n_1, n_2, ..., n_m

        The return of surround() is in the same sequence as `iterable`. For
        the first return of surround() c_1 is equal to the first value
        `iterable` would return. For the last return of surround() c_l is
        equal to the last value `iterable` would return.

    Examples:
        >>> for (prev, curr, next) in surround([1, 2, 3, 4], 1, [2], 2, default=0):
        ...     print(prev, curr, next)
        ...
        0 (1, 2) (3, 4)
        1 (2, 3) (4, 0)
        2 (3, 4) (0, 0)

        >>> it = surround([1, 2, 3, 4], "p1, p2, p3", ["c1, c2"], "n1, n2", default=0)
        >>> for (prev, curr, next) in it:
        ...     print(prev, curr, next)
        ...
        (0, 0, 0) (1, 2) (3, 4)
        (0, 0, 1) (2, 3) (4, 0)
        (0, 1, 2) (3, 4) (0, 0)

    Author:
        Faian Raab <fabian@raab.link>

    Source:
        http://bubblesorted.raab.link/content/iterate-tuple-and-multiple-defaulted-previous-and-next-elements
        https://gitlab.com/snippets/6235 (this)
        https://gitlab.com/snippets/6234 (alternative implementation)
    """
    import itertools
    import sys

    # for python2 compability
    default = kwargs.get('default', None)
    pack_tuple = kwargs.get('pack_tuple', True)

    def get_len(arg):
        if isinstance(arg, int):
            arg_len = arg
        elif isinstance(arg, list):
            arg_len = len(arg)
        elif isinstance(arg, str):
            arg_len = arg.count(',') + 1
        else:
            arg_len = 1

        return arg_len

    # Parse args #
    if len(args) == 1 and isinstance(args[0], str):
        pcn = args[0].replace('[', ']').split(']')
        if len(pcn) != 3:
            raise ValueError("String parameter malformed. "
                             "Are you sure your braces are correct?")
        prev_len = pcn[0].count(',')
        curr_len = pcn[1].count(',') + 1
        next_len = pcn[2].count(',')
        print(pcn, )

    elif len(args) == 1 and isinstance(args[0], int):
        prev_len = 0
        curr_len = args[0]
        next_len = 0

    elif len(args) == 3 and all(isinstance(a, int) for a in args):
        prev_len = get_len(args[0])
        curr_len = get_len(args[1])
        next_len = get_len(args[2])

    else:
        prev_len = 0
        curr_len = 0
        next_len = 0
        is_prev = True

        for arg in args:
            if isinstance(arg, list):
                curr_len = sum(get_len(a) for a in arg)
                is_prev = False
            elif is_prev:
                prev_len += get_len(arg)
            else:
                next_len += get_len(arg)

        if is_prev:
            raise TypeError("Unexpected parameter.")

    # Start Positions #
    # This part is based on @nosklo ideas:
    # http://stackoverflow.com/a/1012089/3644818

    # prev
    prev_it_lst = []
    for i, it in enumerate(itertools.tee(iterable, prev_len)):
        prev_it_lst.append(itertools.chain(
            itertools.repeat(default, prev_len - i),
            it)
        )

    # curr
    curr_it_lst = []
    for i, it in enumerate(itertools.tee(iterable, curr_len)):
        curr_it_lst.append(itertools.islice(it, i, None))

    # next
    next_it_lst = []
    do_break = False
    for i, it in enumerate(itertools.tee(iterable, next_len)):
        next_it_lst.append(
            itertools.chain(
                itertools.islice(it, i + curr_len, None),
                itertools.repeat(default, i + 1)
            )
        )

    # combine iterators #
    if prev_len == 0 and next_len == 0:
        pack_tuple = False

    # for python2 compability
    if sys.version_info < (3,):
        izip = itertools.izip
    else:
        izip = zip

    zips_lst = []
    if pack_tuple:
        if prev_len == 1:
            zips_lst.append(*prev_it_lst)
        elif prev_len > 1:
            zips_lst.append(izip(*prev_it_lst))

        if curr_len == 1:
            zips_lst.append(*curr_it_lst)
        elif curr_len > 1:
            zips_lst.append(izip(*curr_it_lst))

        if next_len == 1:
            zips_lst.append(*next_it_lst)
        elif next_len > 1:
            zips_lst.append(izip(*next_it_lst))

        return izip(*zips_lst)
    else:
        return izip(*(prev_it_lst + curr_it_lst + next_it_lst))
