from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db import *
from datetime import datetime as DateTime

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['StdValuesCache', 'FixTimeStdValuesCache']


class StdValueKey(StdValue):

    """docstring for StdValueKey"""

    def __init__(self, std_value: StdValue, datetime, asn, next=None):
        super(StdValueKey, self).__init__(
            std_value.asn, std_value.val, datetime)
        self.asn = asn
        self.next = next


class StdValuesCache(object):

    def __init__(self, session):
        self.value2row = dict()
        self.session = session

    def get_std_comm(session, std_value: StdValue,
                     source: optional(StdSource)=None,
                     asn: optional(int)=None,
                     datetime: optional(DateTime)=None):

        range_ = StdComm.get_std_comm(
            self.session, std_value, source, asn, datetime)


class FixTimeStdValueKey(StdValue):

    def __init__(self, std_value: StdValue, datetime, asn):
        super(FixTimeStdValueKey, self).__init__(
            std_value.asn, std_value.val, datetime)
        self.asn = asn

    def __hash__(self):
        return (self.asn << 32) + self.value


class FixTimeStdValuesCache(object):

    """docstring for FixTimeStdValuesCache"""

    def __init__(self, session,
                 datetime: DateTime=DateTime.utcnow().replace(microsecond=0)):
        super(FixTimeStdValuesCache, self).__init__()
        self.session = session
        self.datetime = datetime
        self.value2row = dict()

    def get_std_comm(self, std_value: StdValue,
                     source: optional(StdSource)=None,
                     asn: optional(int)=None,
                     options=None,
                     field_load_only=None,):

        if asn is None:
            asn = std_value.asn

        std_value_key = FixTimeStdValueKey(
            std_value, self.datetime, asn)

        range_ = self.value2row.get(hash(std_value_key), None)

        if range_ is not None:
            return range_

        range_ = StdComm.get_std_comm_eager_fields(
            self.session, std_value, source, asn, self.datetime,
            options=options, field_load_only=field_load_only)

        # TODO save if range_ is None
        # if range_ is None:
        #     return None

        std_value_key_insert = FixTimeStdValueKey(
            std_value, self.datetime, asn)

        self.value2row[hash(std_value_key_insert)] = range_

        # if range_ is None:
        #     return None
        # elif isinstance(range_, StdCommRange):
        #     asn = range_.asn
        # elif isinstance(range_, StdCommFieldValue):
        #     asn = range_.range_.asn

        return range_
