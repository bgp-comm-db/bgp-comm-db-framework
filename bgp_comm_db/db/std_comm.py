"""
Classes
"""


from .choice import ChoiceType as MyChoiceType
from abc import ABCMeta
from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db.scope import Ases
from bgp_comm_db.db.scope import Base
from bgp_comm_db.db.scope import IteratorMixin
from bgp_comm_db.db.scope import Scope
from bgp_comm_db.db.std_value import *
from bgp_comm_db.environment import *
from bgp_comm_db.nonemapper import NoneMapper
from bgp_comm_db.surround import surround
from datetime import datetime as DateTime
from datetime import time as Time
from logging import critical
from logging import debug
from logging import error
from logging import info
from logging import warning
from sqlalchemy import Column
from sqlalchemy import desc, asc
from sqlalchemy import distinct
from sqlalchemy import ForeignKey
from sqlalchemy import func
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import BOOLEAN
from sqlalchemy.dialects.mysql import CHAR
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.dialects.mysql import SMALLINT
from sqlalchemy.dialects.mysql import TEXT
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import AbstractConcreteBase
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import aliased
from sqlalchemy.orm import backref
from sqlalchemy.orm import eagerload
from sqlalchemy.orm import joinedload
from sqlalchemy.orm import lazyload
from sqlalchemy.orm import mapper
from sqlalchemy.orm import noload
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import subqueryload
from sqlalchemy.orm import with_polymorphic
from sqlalchemy.schema import Table
import enum
import itertools
import marshmallow as mm
import operator
import sqlalchemy.orm.exc as sa_exc

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['StdComm', 'StdCommRange', 'StdCommField',
           'StdCommFieldValue', 'StdSource']


@enum.unique
class StdSource(enum.Enum):
    unspecific = 1
    rfc = 2
    manual = 3
    xml2007 = 4
    whois = 5
    bgpdump = 6


class StdCommFieldValue(object):

    """docstring for StdCommFieldValue"""

    @classmethod
    def __flat(cls, *args):
        lst = []

        for a in args:
            if isinstance(a, (tuple, list)):
                lst += cls.__flat(*a)
            else:
                lst.append(a)
        return lst

    def __init__(self, range_: 'StdCommRange', *args):
        super(StdCommFieldValue, self).__init__()
        self.range_ = range_

        self.fields = self.__flat(args)

    @property
    def value(self):
        return self.value_factory(self.range_, self.fields)

    @staticmethod
    def value_factory(range_: 'StdCommRange', fields: ['StdCommField']):
        datetime = max(range_.datetime,
                       max(f.datetime for f in fields))
        value = range_.from_value + sum(f.offset for f in fields)
        value.datetime = datetime
        return value

    @property
    def asn(self):
        return self.range_.asn

    @property
    def source(self):
        return self.range_.source

    @property
    def datetime(self):
        return max(self.range_.datetime, max(
            f.datetime for f in self.fields))

    @property
    def description(self):
        string_lst =  [self.range_.description] if self.range_.description is not None else []

        for field in self.fields:
            if field.description is not None:
                string_lst.append(field.description)

        return '; '.join(string_lst)

    def __repr__(self):
        return (repr(self.value) + " <" + repr(self.range_) +
                ": " + repr(self.fields) + ">")


class StdComm(Base, IteratorMixin):

    """docstring for StdCommSuper"""

    __tablename__ = 'std_comm'

    sgk = Column('id', BIGINT, primary_key=True)
    _discriminator = Column('discriminator',
                            ENUM('dummy', 'range', 'field'),
                            nullable=False)

    __mapper_args__ = {'polymorphic_on': _discriminator,
                       'polymorphic_identity': 'dummy'}

    @staticmethod
    @typechecked
    def get_all_ranges(session, as_: union(int, Ases),
                       datetime: DateTime=DateTime.utcnow().replace(
                           microsecond=0),
                       source: optional(StdSource)=None):
        a_range1 = aliased(StdCommRange)
        a_range2 = aliased(StdCommRange)

        if isinstance(as_, int):
            ases = Ases.get_at_datetime(session, as_, datetime)
        else:
            ases = as_

        # TODO replace by StdCommRange.latest_range
        q_result = (session.query(a_range1).
                    outerjoin(a_range2,
                              (a_range1.as_id == a_range2.as_id) &
                              (a_range1._from_as_id == a_range2._from_as_id) &
                              (a_range1._from_val == a_range2._from_val) &
                              (a_range1._to_as_id == a_range2._to_as_id) &
                              (a_range1._to_val == a_range2._to_val) &
                              (a_range1.source == a_range2.source) &
                              (a_range1.datetime < a_range2.datetime)
                              )
                    )

        if source is not None:
            q_result = q_result.filter(a_range1.source == source)

        q_result = (q_result.
                    filter(a_range1.ases == ases,
                           a_range1.datetime <= datetime
                           ).
                    filter(a_range2.as_id == None,
                           a_range2._from_as_id == None,
                           a_range2._from_val == None,
                           a_range2._to_as_id == None,
                           a_range2._to_val == None,
                           a_range2.source == None,
                           )
                    )

        result = q_result.all()

        return result

    @staticmethod
    @typechecked
    def get_std_comm_first(session, std_value: StdValue,
                           source: optional(StdSource)=None,
                           asn: optional(int)=None,
                           datetime: optional(DateTime)=None):
        """

        @deprecated
        """

        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        # TODO replace by StdCommRange.latest_range
        q_range = (session.query(StdCommRange).
                   filter(StdCommRange.datetime <= datetime).
                   order_by(desc(StdCommRange.datetime)).
                   with_transformation(StdCommRange.from_value.transformer).
                   filter(StdCommRange.from_value <= std_value.str_value).
                   with_transformation(StdCommRange.to_value.transformer).
                   filter(std_value.str_value <= StdCommRange.to_value)
                   )

        if source is not None:
            q_range = q_range.filter(StdCommRange.source == source)

        # TODO replace with all()
        range_ = q_range.first()

        if range_ is None or range_.to_value is None:
            # This range have no fields we have to fetch
            return range_

        group_count = (session.query(StdCommField.group).
                       distinct(StdCommField.group).
                       filter(StdCommField.range_ == range_).
                       count()
                       )

        query_tables = []
        addition = range_.from_value.value

        for i in range(group_count):
            a_field1 = aliased(StdCommField)
            a_field2 = aliased(StdCommField)

            subquery = (session.query(a_field1).
                        outerjoin(a_field2,
                                  (a_field1.range_id == a_field2.range_id) &
                                  (a_field1.offset == a_field2.offset) &
                                  (a_field1.group == a_field2.group) &
                                  (a_field1.datetime < a_field2.datetime)
                                  ).
                        filter(a_field1.range_ == range_,
                               a_field1.datetime <= datetime
                               ).
                        filter(a_field2.range_id == None,
                               a_field2.offset == None,
                               a_field2.group == None
                               ).
                        subquery()
                        )

            query_tables.append(aliased(a_field1, subquery))

        query = session.query(*query_tables)

        for i, subq in enumerate(query_tables):
            addition = addition + subq.offset

            if i >= 1:
                query = query.filter(query_tables[i-1].group < subq.group)

        result = query.filter(addition == std_value.value).first()

        if result is None:
            return None

        field_value = StdCommFieldValue(range_, result)

        return field_value

    @typechecked
    def _get_query_options_list(options=None,
                                eager_taxonomies=False,) -> []:
        options_list = []
        if options is None:
            pass
        else:
            try:
                options_list = options_list + list(options)
            except TypeError as _:
                options_list.append(options)

        if eager_taxonomies:
            options_list = options_list + [
                eagerload("localprefs"),
                eagerload("prepends"),
                eagerload("taggings"),
                eagerload("announcements")
            ]

        return options_list

    @staticmethod
    @typechecked
    def get_std_comm(session, std_value: StdValue,
                     source: optional(StdSource)=None,
                     asn: optional(int)=None,
                     datetime: optional(DateTime)=None,
                     options=None,
                     eager_taxonomies=False,) -> []:

        options_list = StdComm._get_query_options_list(
            options,
            eager_taxonomies=eager_taxonomies)

        return StdComm._get_std_comm_op(
            session, std_value, operator.lt,
            source, asn, datetime, options_list)

    @staticmethod
    def get_std_comm_eager_fields(session, std_value: StdValue,
                                  source: optional(StdSource)=None,
                                  asn: optional(int)=None,
                                  datetime: optional(DateTime)=None,
                                  options=None,
                                  field_load_only=None,
                                  eager_taxonomies=False,) -> []:

        options_list = StdComm._get_query_options_list(
            options, eager_taxonomies=eager_taxonomies)

        return StdComm._get_std_comm_eager_fields_op(
            session, std_value, operator.lt,
            source, asn, datetime, options_list, field_load_only)

    @staticmethod
    @typechecked
    def get_std_range(session, std_value: StdValue,
                      source: optional(StdSource)=None,
                      asn: optional(int)=None,
                      datetime: optional(DateTime)=None,
                      options=None,
                      eager_taxonomies=False,) -> []:
        return StdComm._get_std_range_op(
            session, std_value, operator.lt, source, asn, datetime,
            options=options, eager_taxonomies=eager_taxonomies)

    @staticmethod
    def _get_std_comm_eager_fields_op(session, std_value: StdValue, op,
                                      source: optional(StdSource)=None,
                                      asn: optional(int)=None,
                                      datetime: optional(DateTime)=None,
                                      options=None,
                                      field_load_only=None,):
        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        # TODO back to eagerload
        if field_load_only:
            try:
                fields_load = eagerload('fields').load_only(*field_load_only)
            except TypeError as _:
                fields_load = eagerload('fields').load_only(field_load_only)
        else:
            fields_load = eagerload('fields')

        if options is None:
            options = [fields_load]
        else:
            options.append(fields_load)

        range_lst = StdComm._get_std_range_op(
            session, std_value, op, source, asn, datetime, options=options)

        std_comms_lst = []

        for range_ in range_lst:

            if range_ is None:
                pass
            elif range_.to_value is None:
                # This range have no fields we have to fetch
                std_comms_lst.append(range_)
            else:
                field_value = range_.field_value(std_value)

                if field_value is not None:
                    std_comms_lst.append(field_value)

        return std_comms_lst

    @staticmethod
    def _get_std_range_op(session, std_value: StdValue, op,
                          source: optional(StdSource)=None,
                          asn: optional(int)=None,
                          datetime: optional(DateTime)=None,
                          options=None,
                          eager_taxonomies=False,) -> []:

        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        # FIXME remove
        # if (std_value <= StdValue("65000:08928")
        #     or std_value <= StdValue("65003:04134")
        #     or std_value <= StdValue("65000:16509")
        #     or std_value <= StdValue("15412:04602")
        #     or std_value <= StdValue("65503:06453")
        #     or std_value <= StdValue("65000:12956")):
        #     return []

        q_range = (session.query(StdCommRange).
                   filter(StdCommRange.datetime <= datetime).
                   filter(StdCommRange.from_value <= std_value.str_value).
                   filter(StdCommRange.to_value >= std_value.str_value)
                   # FIXME REMOVE
                   # filter(StdCommRange.sgk >= 23458, StdCommRange.sgk <= 24257).
                   # filter(StdCommRange.sgk >= 4671, StdCommRange.sgk <= 14146).
                   # filter(((StdCommRange.sgk >= 23458) & (StdCommRange.sgk <= 43376))
                   #  | (StdCommRange.sgk == 4671) | (StdCommRange.sgk == 14146) |
                   #  (StdCommRange.sgk == 5959)).
                   # filter(StdCommRange.sgk >= 41879, StdCommRange.sgk <= 43376).
                   # filter(StdCommRange.sgk != 5959, StdCommRange.sgk != 41904,
                   #  StdCommRange.sgk != 42345).
                   # filter(StdCommRange.from_as_id == 106451).
            #        with_transformation(
            # StdCommRange.order_datetime(StdCommRange, op)
                                        # )
        )

        if source is not None:
            q_range = q_range.filter(StdCommRange.source == source)

        options_list = StdComm._get_query_options_list(
            options=options, eager_taxonomies=eager_taxonomies)

        if options_list is not None:
            q_range = q_range.options(*options_list)

        # theoretically if sources set, only scalar() should hold. But the DB
        # does not guarantee that, so should we raise an exception?

        # if source is not None:
        #     range_lst = q_range.scalar()
        # else:
        range_lst = q_range.all()

        return range_lst

    @staticmethod
    def _get_std_comm_op(session, std_value: StdValue, op,
                         source: optional(StdSource)=None,
                         asn: optional(int)=None,
                         datetime: optional(DateTime)=None,
                         options=None,):
        if datetime is None:
            datetime = std_value.datetime
        if asn is None:
            asn = std_value.asn

        range_lst = StdComm._get_std_range_op(
            session, std_value, op, source, asn, datetime, options=options)

        std_comms_lst = []

        for range_ in range_lst:

            if range_ is None:
                pass
            elif range_.to_value is None:
                # This range have no fields we have to fetch
                std_comms_lst.append(range_)
            else:
                field_value = StdComm._get_std_comm_field_op(
                    session, std_value, op, range_, source, asn, datetime)

                if field_value is not None:
                    std_comms_lst.append(field_value)

        return std_comms_lst

    @staticmethod
    def _get_std_comm_field_op(session, std_value: StdValue, op, range_,
                               source: optional(StdSource)=None,
                               asn: optional(int)=None,
                               datetime: optional(DateTime)=None):

        group_count = (session.query(StdCommField.group).
                       distinct(StdCommField.group).
                       filter(StdCommField.range_ == range_).
                       count()
                       )

        if group_count <= 0:
            info("%s has no fields." % str(range_))
            return None

        query_tables = []
        addition = range_.from_value.value

        for i in range(group_count):
            a_field = aliased(StdCommField)

            subquery = (session.query(a_field).
                        filter(a_field.range_ == range_,
                               a_field.datetime <= datetime
                               )
                        # .
                        # with_transformation(
                        #     StdCommField.order_datetime(a_field, op))
                        ).subquery()

            query_tables.append(aliased(a_field, subquery))

        query = session.query(*query_tables)

        for i, (prev_subq, subq) in enumerate(
                surround(query_tables, 'prev', ['curr'])):
            addition = addition + subq.offset

            if prev_subq is not None:
                query = query.filter(prev_subq.group < subq.group)

        query = query.filter(addition == std_value.value)

        result = query.all()

        if len(result) > 1:
            raise sa_exc.MultipleResultsFound()

        if result is None or not result:
            return None

        field_value = StdCommFieldValue(range_, result[0])

        return field_value

    class Taxonomy(AbstractConcreteBase, Base, IteratorMixin):

        """docstring for Taxonomy"""
        __metaclass__ = ABCMeta

        std_comm = None

    class TaxonomyMixin(object):

        """docstring for TaxonomyMixin"""

        announcement_scopes = association_proxy('announcements', 'scope')
        prepend_scopes = association_proxy('prepends', 'scope')
        tagging_scopes = association_proxy('taggings', 'scope')
        localpref_scopes = association_proxy('localprefs', 'scope')

        @property
        def taxonomies(self):
            return (self.announcements + self.prepends +
                    self.taggings + self.localprefs)

        def taxonomies_scopes_to_dict(self) -> [{}]:
            l_return = []
            for taxo in self.taxonomies:
                dict_ = {}
                dict_.update(taxo.to_dict())
                del dict_['std_comm_id']
                del dict_['scope_id']

                if (isinstance(taxo, StdComm.Announcement)
                        and taxo.blackhole == False):
                    dict_['taxonomy'] = 'Announce'
                elif ((isinstance(taxo, StdComm.Announcement)
                       and taxo.blackhole == True)):
                    dict_['taxonomy'] = 'Black'
                elif isinstance(taxo, StdComm.LocalPref):
                    dict_['taxonomy'] = 'LocalPref'
                elif isinstance(taxo, StdComm.Tagging):
                    dict_['taxonomy'] = 'Tagging'
                elif isinstance(taxo, StdComm.Prepend):
                    dict_['taxonomy'] = 'Prepend'

                if taxo.scope is not None:
                    dict_.update(taxo.scope.to_dict())
                    dict_['scope'] = taxo.scope.__table__
                del dict_['sgk']
                l_return.append(dict_)
            return l_return


class StdCommRange(StdComm, StdComm.TaxonomyMixin):

    """docstring for StdComm"""

    __tablename__ = 'std_range'

    __to_dict__ = [
        'sgk',
        'as_id',
        'datetime',
        'source',
        'description',
        'from_value',
        'to_value']

    sgk = Column('std_comm_id', BIGINT,
                 ForeignKey('std_comm.id'), primary_key=True)
    as_id = Column(BIGINT, ForeignKey('as.scope_id'))
    _from_as_id = Column('from_as_id', BIGINT, ForeignKey('as.scope_id'),
                         nullable=False)
    _from_val = Column('from', SMALLINT, nullable=False)
    _to_as_id = Column('to_as_id', BIGINT, ForeignKey('as.scope_id'),
                       nullable=False)
    _to_val = Column('to', SMALLINT, nullable=False)
    datetime = Column(DATETIME, nullable=False)
    source = Column(MyChoiceType(StdSource, impl=ENUM(
        'unspecific', 'rfc', 'manual',
        'xml2007', 'whois', 'bgpdump',
        name='StdSource')))
    description = Column(TEXT)
    reference = Column(TEXT)
    observed_only = Column(BOOLEAN, nullable=False, server_default='1')
    field_captures = Column(TEXT)
    ext_field_id = None
    deleted = Column(BOOLEAN)
    fields = None  # backref
    announcements = None  # backref
    prepends = None  # backref
    taggings = None  # backref
    localprefs = None  # backref

    ases = relationship(
        Ases, backref=backref('std_range', lazy=True),
        foreign_keys=[as_id])

    from_ases = relationship(
        Ases, backref=backref('from_std_range', lazy=True),
        foreign_keys=[_from_as_id])

    _to_ases = relationship(
        Ases, backref=backref('to_std_range', lazy=True),
        foreign_keys=[_to_as_id])

    to_ases = NoneMapper.Relationship.hybrid_property('to_as_id', '_to_ases')

    _asn_proxy = association_proxy('ases', 'asn')
    _asn = Column('asn', SMALLINT)
    _as_datetime_proxy = association_proxy('ases', 'datetime')
    _as_datetime = Column('as_datetime', DATETIME)

    _from_asn_proxy = association_proxy('from_ases', 'asn')
    _from_asn = Column('from_asn', SMALLINT)
    _from_as_datetime_proxy = association_proxy('from_ases', 'datetime')
    _from_as_datetime = Column('from_as_datetime', DATETIME)

    _to_asn_proxy = association_proxy('_to_ases', 'asn')
    _to_asn = Column('to_asn', SMALLINT)
    _to_as_datetime_proxy = association_proxy('_to_ases', 'datetime')
    _to_as_datetime = Column('to_as_datetime', DATETIME)

    __mapper_args__ = {'polymorphic_identity': 'range'}

    def __init__(self, from_value: StdValue,
                 datetime: DateTime,
                 source: StdSource,
                 as_id: optional(int)=None, ases: optional(Ases)=None,
                 to_value: optional(StdValue)=None,
                 sgk: optional(int)=None, description: optional(str)=None,
                 reference: optional(str)=None,
                 observed_only: bool=True,
                 field_captures: optional(str)=None,
                 ext_field_id: optional(object)=None,
                 deleted: bool=False):
        super(StdCommRange, self).__init__()

        if ases is not None:
            self.ases = ases
        elif as_id is not None:
            self.as_id = as_id
        else:
            raise TypeError("as_id or ases must be not None.")

        self.from_value = from_value
        self.datetime = datetime
        self.to_value = to_value
        self.sgk = sgk
        self.description = description
        self.source = source
        self.reference = reference
        self.observed_only = observed_only
        self.field_captures = field_captures
        self.ext_field_id = ext_field_id
        self.deleted = deleted
        self._group_numbers = None

    @hybrid_property
    def asn(self):
        return self._asn

    @hybrid_property
    def as_datetime(self):
        return self._as_datetime

    @asn.setter
    def asn(self, value):
        try:
            session = Session()
            ases = Ases.get_at_datetime(session, value, self.datetime)
            self.as_id = ases.sgk
        finally:
            session.close()

    @hybrid_property
    def from_asn(self):
        return self._from_asn

    @hybrid_property
    def from_as_datetime(self):
        return self._from_as_datetime

    # @from_asn.setter
    # def from_asn(self, value):
    #     self._from_asn_proxy = value

    @hybrid_property
    def to_asn(self):
        return self._to_asn

    @hybrid_property
    def to_as_datetime(self):
        return self._to_as_datetime

    # @to_asn.setter
    # def to_asn(self, value):
    #     self._to_asn_proxy = value

    @classmethod
    def latest_entry(cls, orm_cls):
        return cls.order_datetime(orm_cls, operator.lt)

    @classmethod
    def earliest_entry(cls, orm_cls):
        return cls.order_datetime(orm_cls, operator.gt)

    @classmethod
    def order_datetime(cls, orm_cls, op):

        def transform(query):
            a_t_range = aliased(orm_cls.__table__)

            q_result = (query.
                        outerjoin(a_t_range,
                                  (orm_cls.as_id == a_t_range.c.as_id) &
                                  (orm_cls._from_as_id
                                   == a_t_range.c.from_as_id) &
                                  (orm_cls._from_val == a_t_range.c['from']) &
                                  (orm_cls._to_as_id == a_t_range.c.to_as_id) &
                                  (orm_cls._to_val == a_t_range.c['to']) &
                                  (orm_cls.source == a_t_range.c.source) &
                                  op(orm_cls.datetime, a_t_range.c.datetime)
                                  ).
                        filter(a_t_range.c.as_id == None,
                               a_t_range.c.from_as_id == None,
                               a_t_range.c['from'] == None,
                               a_t_range.c.to_as_id == None,
                               a_t_range.c['to'] == None,
                               a_t_range.c.source == None,
                               )
                        )
            return q_result

        return transform

    # @observes('fields')
    # def set_group_numbers(self, fields):
    #     self._group_numbers = set(field.group for field in fields)

    @property
    def group_numbers(self):
        # _group_numbers = getattr(self, '_group_numbers', False)
        # if not _group_numbers:
        self._group_numbers = set(field.group for field in self.fields)
        return self._group_numbers

    @typechecked
    def group(self, numb: int):
        if self.to_value is None or not self.fields:
            raise StopIteration()

        for field in self.fields:
            if field.group == numb:
                yield field

    def field_values(self):
        # TODO: Handle obsoleted fields with new date
        if self.to_value is None or not self.fields:
            raise StopIteration()

        groups = tuple(self.group(g) for g in self.group_numbers)

        for tup in itertools.product(*groups):
            yield StdCommFieldValue(self, *tup)

    def field_value(self, std_value: StdValue):
        # TODO: Handle obsoleted fields with new date
        if self.to_value is None or not self.fields:
            return None

        groups = tuple(self.group(g) for g in self.group_numbers)

        for tup in itertools.product(*groups):
            sval = StdCommFieldValue.value_factory(self, tup)
            if sval <= std_value:
                return StdCommFieldValue(self, *tup)

        return None

    @hybrid_property
    def from_as_id(self):
        return self._from_as_id

    @from_as_id.setter
    def from_as_id(self, value):
        if self.to_as_id is None:
            self._to_as_id = value
        self._from_as_id = value

    @hybrid_property
    def from_val(self):
        return self._from_val

    @from_val.setter
    def from_val(self, value):
        if self.to_val is None:
            self._to_val = value
        self._from_val = value

    @hybrid_property
    def to_as_id(self):
        if (self._from_as_id == self._to_as_id
                and self._from_val == self._to_val):
            return None
        else:
            return self._to_as_id

    @to_as_id.setter
    def to_as_id(self, value):
        if value is None:
            self._to_as_id = self._from_as_id
        else:
            self._to_as_id = value

    @to_as_id.comparator
    def to_as_id(cls):
        return NoneMapper.Comparator(cls._from_as_id, cls._to_as_id)

    @hybrid_property
    def to_val(self):
        if (self._from_val == self._to_val
                and self._from_as_id == self._to_as_id):
            return None
        else:
            return self._to_val

    @to_val.setter
    @typechecked
    def to_val(self, value: optional(int)):
        if value is None:
            self._to_val = self._from_val
        else:
            self._to_val = value

    @to_val.comparator
    def to_val(cls):
        return NoneMapper.Comparator(cls._from_val, cls._to_val)

    @hybrid_property
    def from_value(self) -> StdValue:
        return StdValue(self.from_asn, self.from_val, self.from_as_datetime)

    @from_value.setter
    @typechecked
    def from_value(self, value: StdValue):
        # FIXME if fetching the as id, do not set the val
        self.from_val = value.val
        try:
            session = Session()
            ases = Ases.get_at_datetime(session, value)
            self.from_as_id = ases.sgk
        finally:
            session.close()

    @from_value.comparator
    def from_value(cls):
        """Returns an Comparator for queries.
        """
        return StdValue.Transformer(
            cls, cls._from_asn, cls._from_val,
            cls._from_as_datetime)

    @hybrid_property
    def value(self) -> StdValue:
        if (self._from_as_id == self._to_as_id
                and self._from_val == self._to_val):
            return self.from_value
        else:
            raise Exception("This object has an range.")

    @value.setter
    def value(self, value: StdValue) -> void:
        if (self._from_as_id == self._to_as_id
                and self._from_val == self._to_val):
            self.from_value = value
        else:
            raise Exception("This object has an range.")

    @hybrid_property
    def to_value(self) -> StdValue:
        if self.to_as_id is None and self.to_val is None:
            return None
        else:
            return StdValue(self.to_asn, self.to_val, self.to_as_datetime)

    @to_value.setter
    @typechecked
    def to_value(self, value: optional(StdValue)):
        if value is None:
            self.to_as_id = None
            self.to_val = None
        else:
            self.to_val = value.val
            try:
                session = Session()
                ases = Ases.get_at_datetime(session, value)
                self.to_as_id = ases.sgk
            finally:
                session.close()

    @to_value.comparator
    def to_value(cls):
        """Returns an Comparator for queries.
        """
        return StdValue.TransformerRange(
            cls, cls._to_asn, cls._to_val,
            cls._to_as_datetime)

    def __repr__(self):
        return (
            "<StdCommRange(id=%s, as_id=%s, asn=%s, from_id=%s, from=%s, "
            "to_id=%s, to=%s, %s, %s, %s, descr=%s)>") % (
            self.sgk,
            self.as_id,
            self.asn,
            self.from_as_id,
            self.from_value,
            self.to_as_id,
            self.to_value,
            self.datetime,
            self.source,
            ("active" if not self.deleted else "deleted"),
            self.description)


class StdCommRangeSchema(mm.Schema):

    """docstring for Schema"""
    ases = mm.fields.Nested(Ases.Schema())
    description = mm.fields.Str()
    datetime = mm.fields.DateTime()
    # source = mm.fields.Enum(list(map(lambda x: x.name, list(StdSource))))
    source = mm.fields.Function(lambda obj: obj.source.name)
    reference = mm.fields.Str()
    observed_only = mm.fields.Boolean()
    field_captures = mm.fields.Str()
    fields = mm.fields.Nested('StdCommFieldSchema', many=True, exclude='range')

    # localprefs = mm.fields.Nested('StdCommLocalPrefSchema', many=True)
    taxonomies = mm.fields.Method('get_taxonomies')

    def get_taxonomies(self, obj):
        return_list = []
        for taxo in obj.taxonomies:
            schema = taxo.__class__.Schema()
            result = schema.dump(taxo)
            return_list.append(result.data)
        return return_list

    class Meta:
        ordered = True

StdCommRange.Schema = StdCommRangeSchema

################################ StdCommField #################################


class StdCommField(StdComm, StdComm.TaxonomyMixin):

    """docstring for StdCommSub"""

    __tablename__ = 'std_field'

    sgk = Column('std_comm_id', BIGINT,
                 ForeignKey('std_comm.id'), primary_key=True)
    range_id = Column('std_range_id', BIGINT,
                      ForeignKey('std_range.std_comm_id'), nullable=False)
    offset = Column(SMALLINT, nullable=False)
    group = Column(TINYINT, nullable=False)
    datetime = Column(DATETIME)
    description = Column(TEXT)
    deleted = Column(BOOLEAN)

    range_ = relationship(StdCommRange,
                          backref=backref('fields', lazy=True),
                          foreign_keys=[range_id],
                          lazy='joined')

    __mapper_args__ = {'polymorphic_identity': 'field'}

    def __init__(self, offset: int, group: int, datetime: DateTime,
                 range_id: optional(int)=None,
                 range_: optional(StdCommRange)=None,
                 sgk: optional(int)=None, description: optional(str)=None,
                 deleted: bool=False):
        if range is not None:
            self.range_ = range_
        elif range_id is not None:
            self.as_id = range_id
        else:
            raise TypeError("range_id or range must be not None.")

        self.offset = offset
        self.group = group
        self.sgk = sgk
        self.description = description
        self.datetime = datetime
        self.deleted = deleted

    @classmethod
    def latest_entry(cls, orm_cls):
        return cls.order_datetime(orm_cls, operator.lt)

    @classmethod
    def earliest_entry(cls, orm_cls):
        return cls.order_datetime(orm_cls, operator.gt)

    @classmethod
    def order_datetime(cls, orm_cls, op):

        def transform(query):
            a_t_field = aliased(orm_cls.__table__)

            q_result = (query.
                        outerjoin(a_t_field,
                                  (orm_cls.range_id
                                   == a_t_field.c.std_range_id) &
                                  (orm_cls.offset == a_t_field.c.offset) &
                                  (orm_cls.group == a_t_field.c.group) &
                                  op(orm_cls.datetime, a_t_field.c.datetime)
                                  ).
                        filter(a_t_field.c.std_range_id == None,
                               a_t_field.c.offset == None,
                               a_t_field.c.group == None
                               )
                        )

            return q_result

        return transform

    def __repr__(self):
        return ("<StdCommField(id=%s, std_range=%s, offset=%s, group=%s, "
                "%s, %s, descr=%s)>") % (
            self.sgk, self.range_id, self.offset,
            self.group, self.datetime,
            ("active" if self.deleted == False else "deleted"),
            self.description)


class StdCommFieldSchema(mm.Schema):

    """docstring for Schema"""

    offset = mm.fields.Integer()
    group = mm.fields.Integer()
    datetime = mm.fields.DateTime()
    description = mm.fields.Str()
    range = mm.fields.Nested(
        StdCommRange.Schema(
            exclude=('fields')),
        attribute='range_')

    class Meta:
        ordered = True

StdCommField.Schema = StdCommFieldSchema


################################# LocalPref ###################################


class StdCommLocalPref(StdComm.Taxonomy):

    """docstring for LocalPref"""

    __tablename__ = 'std_localpref'

    __to_dict__ = ['sgk', 'std_comm_id', 'preference', 'scope_id']

    sgk = Column('id', BIGINT, primary_key=True)
    std_comm_id = Column('std_comm_id', BIGINT, ForeignKey('std_comm.id'))
    _scope_id = Column('scope_id', BIGINT,
                       ForeignKey('scope.id'), nullable=False)
    preference = Column(SMALLINT, nullable=False)
    auto_detected = Column(BOOLEAN, server_default='1')
    auto_detected_scope = Column(BOOLEAN, server_default='1')

    std_comm = relationship(StdComm, lazy=False,
                            backref=backref('localprefs', lazy=True)
                            )
    _scope = relationship(Scope, lazy=False,
                          backref=backref('std_comm_localprefs', lazy=True)
                          )

    scope_id = NoneMapper.hybrid_property(1, '_scope_id')
    scope = NoneMapper.Relationship.hybrid_property('scope_id', '_scope')

    def __init__(self, preference: bool, sgk: int=None,
                 std_comm_id: int=None, std_comm: StdComm=None,
                 scope_id: int=None, scope: Scope=None,
                 auto_detected: bool=True, auto_detected_scope: bool=True):
        self.preference = preference
        self.sgk = sgk
        if std_comm is not None:
            self.std_comm = std_comm
        elif std_comm_id is not None:
            self.std_comm_id = std_comm_id
        else:
            raise TypeError("std_comm_id or std_comm must be not None.")

        if scope is not None:
            self.scope = scope
        else:
            self.scope_id = scope_id
        self.auto_detected = auto_detected
        self.auto_detected_scope = auto_detected_scope

    def __repr__(self):
        return ("<StdComm.LocalPref(id=%s, std_comm_id=%s, scope_id=%s, "
                "pref=%s)>") % (self.sgk, self.std_comm_id,
                                self.scope_id, self.preference)

StdComm.LocalPref = StdCommLocalPref


class StdCommLocalPrefSchema(mm.Schema):

    """docstring for Schema"""

    taxonomy = mm.fields.Function(lambda obj: 'localpref')
    preference = mm.fields.Integer()
    auto_detected = mm.fields.Boolean()
    auto_detected_scope = mm.fields.Boolean()
    scope = mm.fields.Method('get_scope')

    def get_scope(self, obj):
        schema = obj.scope.__class__.Schema()
        result = schema.dump(obj.scope)
        return result.data

    class Meta:
        ordered = True

StdCommLocalPref.Schema = StdCommLocalPrefSchema

################################# Announcement ################################


class StdCommAnnouncement(StdComm.Taxonomy):

    """docstring for Announcement"""

    __tablename__ = 'std_announcement'

    __to_dict__ = ['sgk', 'std_comm_id', 'skip', 'blackhole', 'scope_id']

    sgk = Column('id', BIGINT, primary_key=True)
    std_comm_id = Column(BIGINT, ForeignKey('std_comm.id'))
    _scope_id = Column('scope_id', BIGINT,
                       ForeignKey('scope.id'), nullable=False)
    skip = Column(BOOLEAN, nullable=False)
    blackhole = Column(BOOLEAN, server_default='0')
    auto_detected = Column(BOOLEAN, server_default='1')
    auto_detected_scope = Column(BOOLEAN, server_default='1')

    std_comm = relationship(StdComm, lazy=False,
                            backref=backref('announcements', lazy=True)
                            )
    _scope = relationship(Scope, lazy=False,
                          backref=backref('std_comm_announcements', lazy=True)
                          )

    scope_id = NoneMapper.hybrid_property(1, '_scope_id')
    scope = NoneMapper.Relationship.hybrid_property('scope_id', '_scope')

    def __init__(self, skip: bool, blackhole: bool, sgk: int=None,
                 std_comm_id: int=None, std_comm: StdComm=None,
                 scope_id: int=None, scope: Scope=None,
                 auto_detected: bool=True, auto_detected_scope: bool=True):
        self.skip = skip
        self.blackhole = blackhole
        self.sgk = sgk
        if std_comm is not None:
            self.std_comm = std_comm
        elif std_comm_id is not None:
            self.std_comm_id = std_comm_id
        else:
            raise TypeError("std_comm_id or std_comm must be not None.")

        if scope is not None:
            self.scope = scope
        else:
            self.scope_id = scope_id
        self.auto_detected = auto_detected
        self.auto_detected_scope = auto_detected_scope

    def __repr__(self):
        return ("<StdComm.Announcement(id=%s, std_comm=%s, scope=%s, "
                "skip=%s, blackhole=%s)>") % (self.sgk, self.std_comm_id,
                                              self.scope_id, self.skip,
                                              self.blackhole)

# make class nested
StdComm.Announcement = StdCommAnnouncement


class StdCommAnnouncementSchema(mm.Schema):

    """docstring for Schema"""

    taxonomy = mm.fields.Function(lambda obj: 'announcement')
    skip = mm.fields.Boolean()
    blackhole = mm.fields.Boolean()
    auto_detected = mm.fields.Boolean()
    auto_detected_scope = mm.fields.Boolean()
    scope = mm.fields.Method('get_scope')

    def get_scope(self, obj):
        schema = obj.scope.__class__.Schema()
        result = schema.dump(obj.scope)
        return result.data

    class Meta:
        ordered = True

StdCommAnnouncement.Schema = StdCommAnnouncementSchema


################################### Prepend ###################################


class StdCommPrepend(StdComm.Taxonomy):

    """docstring for StdCommPrepend"""

    __tablename__ = 'std_prepend'

    __to_dict__ = ['sgk', 'std_comm_id', 'times', 'scope_id']

    sgk = Column('id', BIGINT, primary_key=True)
    std_comm_id = Column(BIGINT, ForeignKey('std_comm.id'))
    _scope_id = Column('scope_id', BIGINT,
                       ForeignKey('scope.id'), nullable=False)
    times = Column(SMALLINT)
    auto_detected = Column(BOOLEAN, server_default='1')
    auto_detected_scope = Column(BOOLEAN, server_default='1')

    std_comm = relationship(StdComm, lazy=False,
                            backref=backref('prepends', lazy=True)
                            )
    _scope = relationship(Scope, lazy=False,
                          backref=backref('std_comm_prepends', lazy=True)
                          )

    scope_id = NoneMapper.hybrid_property(1, '_scope_id')
    scope = NoneMapper.Relationship.hybrid_property('scope_id', '_scope')

    def __init__(self, times: int, sgk: int=None,
                 std_comm_id: int=None, std_comm: StdComm=None,
                 scope_id: int=None, scope: Scope=None,
                 auto_detected: bool=True, auto_detected_scope: bool=True):
        self.times = times
        self.sgk = sgk
        if std_comm is not None:
            self.std_comm = std_comm
        elif std_comm_id is not None:
            self.std_comm_id = std_comm_id
        else:
            raise TypeError("std_comm_id or std_comm must be not None.")

        if scope is not None:
            self.scope = scope
        else:
            self.scope_id = scope_id
        self.auto_detected = auto_detected
        self.auto_detected_scope = auto_detected_scope

    def __repr__(self):
        return ("<StdComm.Prepend(id=%s, std_comm=%s, scope=%s, "
                "times=%s)>") % (self.sgk, self.std_comm_id, self.scope_id,
                                 self.times)

# make class nested
StdComm.Prepend = StdCommPrepend


class StdCommPrependSchema(mm.Schema):

    """docstring for Schema"""

    taxonomy = mm.fields.Function(lambda obj: 'prepend')
    times = mm.fields.Integer()
    auto_detected = mm.fields.Boolean()
    auto_detected_scope = mm.fields.Boolean()
    scope = mm.fields.Method('get_scope')

    def get_scope(self, obj):
        schema = obj.scope.__class__.Schema()
        result = schema.dump(obj.scope)
        return result.data

    class Meta:
        ordered = True

StdCommPrepend.Schema = StdCommPrependSchema

################################### Tagging ###################################


class StdCommTagging(StdComm.Taxonomy):

    """docstring for StdCommTagging"""

    __tablename__ = 'std_tagging'

    __to_dict__ = ['sgk', 'std_comm_id', 'scope_id']

    sgk = Column('id', BIGINT, primary_key=True)
    std_comm_id = Column(BIGINT, ForeignKey('std_comm.id'))
    _scope_id = Column('scope_id', BIGINT,
                       ForeignKey('scope.id'), nullable=False)
    auto_detected = Column(BOOLEAN, server_default='1')
    auto_detected_scope = Column(BOOLEAN, server_default='1')

    std_comm = relationship(StdComm, lazy=False,
                            backref=backref('taggings', lazy=True)
                            )
    _scope = relationship(Scope, lazy=False,
                          backref=backref('std_comm_taggings', lazy=True)
                          )

    scope_id = NoneMapper.hybrid_property(1, '_scope_id')
    scope = NoneMapper.Relationship.hybrid_property('scope_id', '_scope')

    def __init__(self, sgk: int=None,
                 std_comm_id: int=None, std_comm: StdComm=None,
                 scope_id: int=None, scope: Scope=None,
                 auto_detected: bool=True, auto_detected_scope: bool=True):
        self.sgk = sgk
        if std_comm is not None:
            self.std_comm = std_comm
        elif std_comm_id is not None:
            self.std_comm_id = std_comm_id
        else:
            raise TypeError("std_comm_id or std_comm must be not None.")

        if scope is not None:
            self.scope = scope
        else:
            self.scope_id = scope_id
        self.auto_detected = auto_detected
        self.auto_detected_scope = auto_detected_scope

    def __repr__(self):
        return "<StdComm.Tagging(id=%s, std_comm=%s, scope=%s)>" % (
            self.sgk, self.std_comm_id, self.scope_id)

# make class nested
StdComm.Tagging = StdCommTagging


class StdCommTaggingSchema(mm.Schema):

    """docstring for Schema"""

    taxonomy = mm.fields.Function(lambda obj: 'tagging')
    auto_detected = mm.fields.Boolean()
    auto_detected_scope = mm.fields.Boolean()
    scope = mm.fields.Method('get_scope')

    def get_scope(self, obj):
        schema = obj.scope.__class__.Schema()
        result = schema.dump(obj.scope)
        return result.data

    class Meta:
        ordered = True

StdCommTagging.Schema = StdCommTaggingSchema

################################### Include ###################################

std_including_table = Table('std_including', Base.metadata,
                            Column('std_include_id', BIGINT,
                                   ForeignKey('std_include.id')),
                            Column('std_comm_id', BIGINT,
                                   ForeignKey('std_comm.id')))


class StdCommInclude(StdComm.Taxonomy):

    """docstring for StdCommInclude"""

    __tablename__ = 'std_include'

    __to_dict__ = ['sgk', 'std_comm_id', 'scope_id']

    sgk = Column('id', BIGINT, primary_key=True)
    std_comm_id = Column(BIGINT, ForeignKey('std_comm.id'))
    _scope_id = Column('scope_id', BIGINT,
                       ForeignKey('scope.id'), nullable=False)
    auto_detected = Column(BOOLEAN, server_default='1')
    auto_detected_scope = Column(BOOLEAN, server_default='1')

    including = relationship(
        StdComm,
        secondary=std_including_table,
        backref="included_by")

    std_comm = relationship(StdComm, lazy=False,
                            backref=backref('includes', lazy=True),
                            foreign_keys=[std_comm_id]
                            )
    _scope = relationship(Scope, lazy=False,
                          backref=backref('std_comm_includes', lazy=True)
                          )

    scope_id = NoneMapper.hybrid_property(1, '_scope_id')
    scope = NoneMapper.Relationship.hybrid_property('scope_id', '_scope')

    def __init__(self, sgk: optional(int)=None,
                 std_comm_id: optional(int)=None,
                 std_comm: optional(StdComm)=None,
                 scope_id: int=None, scope: Scope=None,
                 std_comm_include_id: int=None, std_comm_include: StdComm=None,
                 auto_detected: bool=True, auto_detected_scope: bool=True):
        self.sgk = sgk
        if std_comm is not None:
            self.std_comm = std_comm
        elif std_comm_id is not None:
            self.std_comm_id = std_comm_id
        else:
            raise TypeError("std_comm_id or std_comm must be not None.")

        if scope is not None:
            self.scope = scope
        else:
            self.scope_id = scope_id

        if std_comm_include is not None:
            self.std_comm_include = std_comm_include
        else:
            self.std_comm_include_id = std_comm_include_id

        self.auto_detected = auto_detected
        self.auto_detected_scope = auto_detected_scope

    def __repr__(self):
        return ("<StdComm.Include(id=%s, std_comm=%s, "
                "scope=%s)>") % (
            self.sgk, self.std_comm_id,
            self.scope_id)

# make class nested
StdComm.Include = StdCommInclude


class StdCommIncludeSchema(mm.Schema):

    """docstring for Schema"""

    taxonomy = mm.fields.Function(lambda obj: 'tagging')
    auto_detected = mm.fields.Boolean()
    auto_detected_scope = mm.fields.Boolean()
    scope = mm.fields.Method('get_scope')
    include = mm.fields.Method('get_include')

    fields = mm.fields.Nested('StdCommFieldSchema', many=True, exclude='range')

    def get_scope(self, obj):
        schema = obj.scope.__class__.Schema()
        result = schema.dump(obj.scope)
        return result.data

    def get_include(self, obj):
        # TODO correct schema, not only description
        schema = obj.std_comm_include.__class__.Schema(only=('description'))
        result = schema.dump(obj.scope)
        return result.data

    class Meta:
        ordered = True

StdCommTagging.Include = StdCommIncludeSchema
