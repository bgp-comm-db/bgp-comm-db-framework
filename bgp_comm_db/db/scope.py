'''
Class Scope
'''

from .choice import ChoiceType as MyChoiceType
from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db.std_value import *
from bgp_comm_db.nonemapper import NoneMapper
from datetime import datetime as DateTime
from logging import critical
from logging import debug
from logging import error
from logging import exception
from logging import info
from logging import log
from logging import warning
from marshmallow import fields
from marshmallow import Schema
from sqlalchemy import and_
from sqlalchemy import Column
from sqlalchemy import desc
from sqlalchemy import ForeignKey
from sqlalchemy import inspect
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import BOOLEAN
from sqlalchemy.dialects.mysql import CHAR
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.dialects.mysql import SMALLINT
from sqlalchemy.dialects.mysql import TEXT
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship
from sqlalchemy.orm import Session
from sqlalchemy.orm import sessionmaker
import abc
import enum
import sqlalchemy.ext.hybrid

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = [
    'Scope',
    'Ases',
    'Peertype',
    'PeertypeEnum',
    'Ixp',
    'Geographic',
    'Ipv4',
    'Ipv6',
    'IteratorMixin']


class Base(object):

    """docstring for Base"""

    __to_dict__ = None

    @abc.abstractmethod
    def columns_to_dict(self) -> {}:
        dict_ = {}
        for key in self.__mapper__.c.keys():
            dict_[key] = getattr(self, key)
        return dict_

    @abc.abstractmethod
    def hybrid_properties_to_dict(self) -> {}:
        dict_ = {}
        for key, prop in inspect(self.__class__).all_orm_descriptors.items():
            if isinstance(prop, hybrid_property):
                dict_[key] = getattr(self, key)
        return dict_

    @abc.abstractmethod
    def publics_to_dict(self) -> {}:
        dict_ = {}
        for key in self.__mapper__.c.keys():
            if not key.startswith('_'):
                dict_[key] = getattr(self, key)

        dict_.update(self.hybrid_properties_to_dict())
        return dict_

    @abc.abstractmethod
    def to_dict(self, attributes: optional([str])=None) -> {}:
        if attributes is None:
            attributes = self.__to_dict__
        if attributes is None:
            return self.publics_to_dict()
        else:
            dict_ = {}
            for entry in attributes:
                dict_[entry] = getattr(self, entry)
            return dict_

Base = declarative_base(cls=Base)


class IteratorMixin(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def columns_generator(self):
        for key in self.__mapper__.c.keys():
            yield getattr(self, key)

    @abc.abstractmethod
    def hybrid_properties_generator(self):
        for key, prop in inspect(self.__class__).all_orm_descriptors.items():
            if isinstance(prop, hybrid_property):
                yield getattr(self, key)

    @abc.abstractmethod
    def publics_generator(self):
        for key in self.__mapper__.c.keys():
            if not key.startswith('_'):
                yield getattr(self, key)

        yield from self.hybrid_properties_generator()

    @abc.abstractmethod
    def generator(self, attributes: optional([str])=None):
        if attributes is None:
            attributes = self.__to_dict__
        if attributes is None:
            yield from self.publics_generator()
        else:
            for entry in attributes:
                yield getattr(self, entry)

    # TODO on commit() function test_create_an_include() raise error
    # AttributeError: 'int' object has no attribute 'fields'. SQLalchemy seems
    # to use the iterator when defined, but confuses with unexpected returns
    # @abc.abstractmethod
    # def __iter__(self):
    #     yield from self.generator(attributes=None)


class Scope(Base, IteratorMixin):

    """docstring for Scope"""

    __tablename__ = 'scope'

    sgk = Column('id', BIGINT, primary_key=True)
    _discriminator = Column('discriminator', ENUM('dummy', 'as', 'ixp',
                                                  'geographic', 'peertype',
                                                  'ipv4', 'ipv6'),
                            nullable=False)

    __mapper_args__ = {'polymorphic_on': _discriminator,
                       'polymorphic_identity': 'dummy'}

    def __repr__(self):
        return "<Scope(id='%s')>" % (self.sgk)


@enum.unique
class PeertypeEnum(enum.Enum):

    """docstring for PeertypeEnum"""
    peer = 1
    customer = 2
    non_customer = 3
    provider = 4
    transit = 5
    internal = 6
    internal_more_specific = 7
    global_ = 8
    border = 9
    upstream = 10
    special_purpose = 11
    stub = 12
    multihomed = 13

    def __str__(self):
        if self == PeertypeEnum.peer:
            return "any peer"
        elif self == PeertypeEnum.non_customer:
            return "non-customer peer"
        string = self.name

        if string.endswith('_'):
            string = string[:-1]
        string = string.replace('_', ' ')
        string = string + " peer"

        return string

PeertypeEnum.non_customer.dbvalue = 'non-customer'
PeertypeEnum.global_.dbvalue = 'global'


class Peertype(Scope):

    """docstring for Peertype"""

    __tablename__ = 'peertype'

    __to_dict__ = ['sgk', 'type_']

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 primary_key=True, server_default='0')
    type_ = Column(
        'type',
        MyChoiceType(PeertypeEnum, impl=VARCHAR(30)),
        nullable=False)
    description = Column(TEXT)

    __mapper_args__ = {'polymorphic_identity': 'peertype'}

    def __repr__(self):
        return "<Peertype(scope_id=%s, type='%s')>" % (
            self.sgk, self.type_)

    class Schema(Schema):

        """docstring for Schema"""

        scope = fields.Function(lambda obj: obj.__class__.__tablename__)
        type = fields.Str()

        class Meta:
            ordered = True


class Geographic(Scope):

    """docstring for Geographic"""

    __tablename__ = 'geographic'

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 primary_key=True, server_default='0')
    _continent = Column('continent', CHAR(2), nullable=False)
    _country = Column('country', CHAR(2))
    _subdivision = Column('subdivision', CHAR(3))
    _location = Column('location', CHAR(3))
    _district = Column('district', VARCHAR(45))

    __mapper_args__ = {'polymorphic_identity': 'geographic'}

    __to_dict__ = ['sgk', 'geo_short']

    def __init__(self, continent, sgk=None, country=None, subdivision=None,
                 location=None, district=None):
        super(Geographic, self).__init__()
        self.continent = continent
        self.country = country
        self.subdivision = subdivision
        self.location = location
        self.district = district

    def __repr__(self):
        return "<Geo(scope_id=%s, Location: %s)>" % (
            self.sgk, self.geo_short)

    @hybrid_property
    def geo_short(self) -> str:
        return "%s-%s-%s-%s-%s" % (
            self.continent, self.country, self.subdivision,
            self.location, self.district)

    @geo_short.setter
    def geo_short(self, value: str):
        (continent,
         country,
         subdivision,
         location,
         district) = value.split('-')

        if continent == 'None':
            self.continent = None
        elif continent not in ['?', '.', '*', '']:
            self.continent = continent

        if country == 'None':
            self.country = None
        elif country not in ['?', '.', '*', '']:
            self.country = country

        if subdivision == 'None':
            self.subdivision = None
        elif subdivision not in ['?', '.', '*', '']:
            self.subdivision = subdivision

        if location == 'None':
            self.location = None
        elif location not in ['?', '.', '*', '']:
            self.location = location

        if district == 'None':
            self.district = None
        elif district not in ['?', '.', '*', '']:
            self.district = district

    @geo_short.comparator
    def geo_short(cls):
        return Geographic.ShortComparator(None)

    @hybrid_property
    def continent(self) -> str:
        return self._continent

    @continent.setter
    @typechecked
    def continent(self, value: optional(union(int, str))) -> void:

        rfc4384 = [(0b00001, 'AF'),
                   (0b00010, 'OC'),
                   (0b00011, 'AS'),
                   (0b00100, 'AN'),
                   (0b00101, 'EU'),
                   (0b00110, 'SA'),
                   (0b00111, 'NA'),
                   # RFC4384:
                   # Antarctica (AQ) == AN
                   # Latin America/Caribbean Islands (LOC) == SA
                   ('AQ', 'AN'),
                   ('LOC', 'SA')]

        for tup in rfc4384:
            if tup[0] == value:
                value = tup[1]

        # AF - Africa
        # AS - Asia
        # EU - Europe
        # NA - North America
        # SA - South America
        # OC - Oceania
        # AN - Antarctica

        assert(value in ['AF', 'AS', 'EU', 'NA', 'SA', 'OC', 'AN'])
        self._continent = value

    country = NoneMapper.hybrid_property('', '_country')

    subdivision = NoneMapper.hybrid_property('', '_subdivision',
                                             mapping_allowed=True)

    location = NoneMapper.hybrid_property('', '_location')

    district = NoneMapper.hybrid_property('', '_district')

    class ShortComparator(sqlalchemy.ext.hybrid.Comparator):

        """docstring for ShortComparator"""

        def __eq__(self, other):
            (continent,
             country,
             subdivision,
             location,
             district) = other.split('-')

            if continent == 'None':
                continent_expr = Geographic.continent == None
            elif continent not in ['?', '.', '*', '']:
                continent_expr = Geographic.continent == continent
            else:
                continent_expr = True

            if country == 'None':
                country_expr = Geographic.country == None
            elif country not in ['?', '.', '*', '']:
                country_expr = Geographic.country == country
            else:
                country_expr = True

            if subdivision == 'None':
                subdivision_expr = Geographic.subdivision == None
            elif subdivision not in ['?', '.', '*', '']:
                subdivision_expr = Geographic.subdivision == subdivision
            else:
                subdivision_expr = True

            if location == 'None':
                location_expr = Geographic.location == None
            elif location not in ['?', '.', '*', '']:
                location_expr = Geographic.location == location
            else:
                location_expr = True

            if district == 'None':
                district_expr = Geographic.district == None
            elif district not in ['?', '.', '*', '']:
                district_expr = Geographic.district == district
            else:
                district_expr = True

            return (continent_expr & country_expr &
                    subdivision_expr & location_expr & district_expr)

    class Schema(Schema):

        """docstring for Schema"""

        scope = fields.Function(lambda obj: obj.__class__.__tablename__)
        continent = fields.Str()
        country = fields.Str()
        subdivision = fields.Str()
        location = fields.Str()
        district = fields.Str()

        class Meta:
            ordered = True


class Ixp(Scope):

    """docstring for Ixp"""

    __tablename__ = 'ixp'

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 primary_key=True, server_default='0')
    name = Column(VARCHAR(40), nullable=False)
    nicename = Column(TEXT)
    geographic_id = Column(BIGINT, ForeignKey('geographic.scope_id'))

    geographic = relationship(
        Geographic, backref=backref('ixps', lazy='dynamic'),
        foreign_keys=[geographic_id])

    __mapper_args__ = {'polymorphic_identity': 'ixp'}

    def __repr__(self):
        return "<IXP(scope_id=%s, name='%s', geographic_id=%s)>" % (
            self.sgk, self.name, self.geographic_id)

    class Schema(Schema):

        """docstring for Schema"""

        scope = fields.Function(lambda obj: obj.__class__.__tablename__)
        name = fields.Str()
        nicename = fields.Str()
        geographic = fields.Nested(Geographic.Schema())

        class Meta:
            ordered = True


class Ases(Scope):

    """docstring for AS"""

    __tablename__ = 'as'

    __to_dict__ = ['sgk', 'asn', 'datetime', 'name']

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 server_default='0', primary_key=True)
    asn = Column(INTEGER)
    datetime = Column(DATETIME)
    name = Column(VARCHAR(70))
    nicename = Column(TEXT)
    geographic_id = Column(BIGINT, ForeignKey('geographic.scope_id'))
    comm_doc_url = Column(TEXT)

    __mapper_args__ = {'polymorphic_identity': 'as'}

    geographic = relationship(
        Geographic, backref=backref('ases', lazy='dynamic'),
        foreign_keys=[geographic_id])

    @staticmethod
    def get_at_datetime(session: Session,
                        asn_or_std_value: union(int, StdValue),
                        datetime: optional(DateTime)=None) -> "Ases":
        """Returns the AS with asn which is valid at the given date.

        Args:
            session: A started session where an query will be invoked.
            asn_or_std_value: The AS number which you are interested for. Or
                              an StdValue object, which carries asn and
                              datetime.
            datetime: Any datetime after 1970-01-01 00:00:00,
                      where you want to have an valid AS object.

        Returns:
            An Ases instance from the session which is valid at datetime.
        """
        if isinstance(asn_or_std_value, StdValue):
            return Ases.get_at_datetime(session, asn_or_std_value.asn,
                                        asn_or_std_value.datetime)

        if datetime is None:
            datetime = DateTime.utcnow().replace(microsecond=0)

        ases = (session.query(Ases).
                filter(Ases.asn == asn_or_std_value).
                filter(Ases.datetime <= datetime).
                order_by(desc(Ases.datetime)).first())

        if ases is None:
            ases = Ases(asn=asn_or_std_value,
                        datetime=datetime,
                        name="UNKNOWN",
                        nicename="UNKNOWN. This AS is auto generated. "
                        "Maybe it was simply found in a message.")
            session.add(ases)
            session.commit()

            warning("AS%d was not found in DB. "
                    "A now Entry was auto generated: %s",
                    asn_or_std_value, ases)

        debug("Fetch AS: %s", ases)
        return ases

    def __repr__(self):
        return "<AS(scope_id=%s, asn=%s, date=%s, name='%s')>" % (
            self.sgk, self.asn, self.datetime, self.name)

    class Schema(Schema):

        """docstring for Schema"""

        scope = fields.Function(lambda obj: obj.__class__.__tablename__)
        asn = fields.Integer()
        datetime = fields.DateTime()
        name = fields.Str()
        nicename = fields.Str()
        geographic = fields.Nested(Geographic.Schema(), exclude=('scope'))

        class Meta:
            ordered = True


class Ipv4(Scope):

    """docstring for Ipv4"""

    __tablename__ = 'ipv4'

    __to_dict__ = ['sgk', 'ip4']

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 primary_key=True, server_default='0')
    ip4 = Column('ip', INTEGER, nullable=False)
    as_id = Column(BIGINT, ForeignKey('as.scope_id'))

    __mapper_args__ = {'polymorphic_identity': 'ipv4'}

    ases = relationship(
        Ases, backref=backref('scope_ipv4', lazy='dynamic'),
        foreign_keys=[as_id])

    asn = association_proxy('ases', 'asn')

    def __repr__(self):
        return "<IPv4(scope_id=%s, ip4=%s, as_id=%s)>" % (
            self.sgk, self.ip4, self.as_id)

    class Schema(Schema):

        """docstring for Schema"""

        ip4 = fields.Str()
        ases = fields.Nested(Ases.Schema())

        class Meta:
            ordered = True


class Ipv6(Scope):

    """docstring for Ipv6"""

    __tablename__ = 'ipv6'

    __to_dict__ = ['sgk', 'ip6']

    sgk = Column('scope_id', BIGINT, ForeignKey('scope.id'),
                 primary_key=True, server_default='0')
    ip6 = Column('ip', CHAR(16), nullable=False)
    as_id = Column(BIGINT, ForeignKey('as.scope_id'))

    __mapper_args__ = {'polymorphic_identity': 'ipv6'}

    ases = relationship(
        Ases, backref=backref('scope_ipv6', lazy='dynamic'),
        foreign_keys=[as_id])

    asn = association_proxy('ases', 'asn')

    def __repr__(self):
        return "<IPv4(scope_id=%s, ip6=%s, as_id=%s)>" % (
            self.sgk, self.ip6, self.as_id)

    class Schema(Schema):

        """docstring for Schema"""

        ip6 = fields.Str()
        ases = fields.Nested(Ases.Schema())

        class Meta:
            ordered = True
