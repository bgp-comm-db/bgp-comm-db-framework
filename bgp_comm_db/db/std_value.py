"""
StdValue Class.
"""

from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from datetime import datetime as DateTime
from sqlalchemy import asc
from sqlalchemy import desc
from sqlalchemy.sql import bindparam
from sqlalchemy.sql.expression import BinaryExpression
import sqlalchemy.ext.hybrid

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['StdValue']


class StdValue(object):

    """
    Represents a single BGP Standard Community.

        75234:13456
        `asn´ `val´
        `--value--´
    """

    @staticmethod
    def parse_value(asn_or_value: union(int, str),
                    val: optional(union(int, str))=None):
        # TODO remove again?
        if asn_or_value is None:
            return (None, None)

        ret_asn = None
        ret_val = None

        if isinstance(asn_or_value, str) and ':' in asn_or_value:
            if val != None:
                raise Exception("Unexpected parameter 'val'. "
                                " 'val' should be None.")

            (asn, val) = asn_or_value.strip().split(':')

            ret_asn = int(asn)
            ret_val = int(val)
        elif val == None:
            if isinstance(asn_or_value, str):
                if int(asn_or_value, 0) > 2**32:
                    raise Exception(
                        "'value' not valid (> 2^32): ", asn_or_value)

                ret_asn = (int(asn_or_value, 0) >> 16) & 0xffff
                ret_val = int(asn_or_value, 0) & 0xffff
            else:
                if asn_or_value > 2**32:
                    raise Exception(
                        "'value' not valid (> 2^32): ", asn_or_value)

                ret_asn = (asn_or_value >> 16) & 0xffff
                ret_val = asn_or_value & 0xffff

        else:
            if isinstance(asn_or_value, str):
                ret_asn = int(asn_or_value, 0)
            else:
                ret_asn = asn_or_value
            if isinstance(val, str):
                ret_val = int(val, 0)
            else:
                ret_val = val

        if ret_asn is not None and ret_asn > 2 ** 16:
            raise ValueError("Invalid ASN (> 2^16)", ("%s:%s") %
                             (ret_asn, ret_val))
        if ret_val is not None and ret_val > 2 ** 16:
            raise ValueError("Invalid val (> 2^16)", ("%s:%s") %
                             (ret_asn, ret_val))

        return (ret_asn, ret_val)

    def __init__(self, asn_or_value: union(int, str),
                 val_or_datetime: optional(union(int, str, DateTime))=None,
                 datetime: DateTime=DateTime.utcnow().replace(microsecond=0)):
        self.asn = None
        self.val = None

        if isinstance(val_or_datetime, DateTime):
            self.datetime = val_or_datetime
            val = None
        else:
            self.datetime = datetime
            val = val_or_datetime

        (self.asn, self.val) = StdValue.parse_value(asn_or_value, val)

    def __iter__(self):
        yield self.str_value
        yield self.datetime

        for key, val in self.__dict__.items():
            if key == 'val' or key == 'asn' or key == 'datetime':
                continue
            yield val

    def to_dict(self):
        return_dict = dict()
        return_dict.update({'value': self.str_value})
        return_dict.update({'datetime': self.datetime})

        for key, value in self.__dict__.items():
            if key == 'val' or key == 'asn' or key == 'datetime':
                continue
            return_dict.update({key: value})
        return return_dict

    @property
    def is_reserved(self):
        return (self.asn == 0) or (self.asn == 65535) or self.is_private or self.is_documentation

    @property
    def is_private(self):
        """Indicates if this is a private use ASN (RFC6996)"""
        return 64512 <= self.asn <= 65534

    @property
    def is_documentation(self):
        """Indicates if this is a documentation and sample code ASN (RFC5398)"""
        return 64496 <= self.asn <= 64511

    @property
    def is_astrans(self):
        return self.asn == 23456

    def __int__(self):
        return (self.asn << 16) + self.val

    @property
    def value(self):
        return int(self)

    @value.setter
    def value(self, value):
        self.asn = (value >> 16) & 0xffff
        self.val = value & 0xffff

    def __eq__(self, other):
        if isinstance(other, StdValue):
            return (self.asn == other.asn and self.val ==
                    other.val and self.datetime == other.datetime)
        elif isinstance(other, str):
            std_value = StdValue(other)
            return int(self) == int(std_value)
        else:
            return int(self) == other

    def __ne__(self, other):
        return not(self == other)

    def __lt__(self, other):
        if isinstance(other, StdValue):
            return (self.asn == other.asn and self.val ==
                    other.val and self.datetime < other.datetime)
        elif isinstance(other, str):
            std_value = StdValue(other)
            return int(self) < int(std_value)
        else:
            return int(self) < other

    def __le__(self, other):
        if isinstance(other, StdValue):
            return (self.asn == other.asn and self.val ==
                    other.val and self.datetime <= other.datetime)
        elif isinstance(other, str):
            std_value = StdValue(other)
            return int(self) <= int(std_value)
        else:
            return int(self) <= other

    def __gt__(self, other):
        if isinstance(other, StdValue):
            return ((self.asn == other.asn) and
                    (self.val == other.val) and
                    (self.datetime > other.datetime))
        elif isinstance(other, str):
            std_value = StdValue(other)
            return int(self) > int(std_value)
        else:
            return int(self) > other

    def __ge__(self, other):
        if isinstance(other, StdValue):
            return ((self.asn == other.asn) and
                    (self.val == other.val) and
                    (self.datetime >= other.datetime))
        elif isinstance(other, str):
            std_value = StdValue(other)
            return int(self) >= int(std_value)
        else:
            return int(self) >= other

    def __add__(self, other):
        result = int(self) + int(other)
        return StdValue(result, self.datetime)

    def __iadd__(self, other):
        self.value = int(self) + int(other)
        return self

    def __sub__(self, other):
        result = int(self) - int(other)
        return StdValue(result, self.datetime)

    def __isub__(self, other):
        self.value = int(self) - int(other)
        return self

    def __bool__(self):
        return self.asn != None and self.val != None

    # def __hash__(self):
    #     return hash(self.value)

    def __repr__(self):
        if self.asn is None and self.val is None:
            return "<StdValue(?????:?????, %s)>" % (
                self.datetime)
        elif self.asn is None:
            return "<StdValue(?????:{0:05d}, %s)>".format(self.asn) % (
                self.datetime)
        elif self.val is None:
            return "<StdValue({0:05d}:?????, %s)>".format(self.val) % (
                self.datetime)
        else:
            return "<StdValue({0:05d}:{1:05d}, %s)>".format(self.asn,
                                                            self.val) % (
                self.datetime)

    def __str__(self):
        if self.asn is None and self.val is None:
            return "?????:????? (%s)" % (
                self.datetime)
        elif self.asn is None:
            return "?????:{0:05d} (%s)".format(self.val) % (
                self.datetime)
        elif self.val is None:
            return "{0:05d}:????? (%s)".format(self.asn) % (
                self.datetime)
        else:
            return "{0:05d}:{1:05d} (%s)".format(self.asn, self.val) % (
                self.datetime)

    @property
    def str_value(self) -> str:
        if self.asn is None and self.val is None:
            return "?????:?????"
        elif self.asn is None:
            return "?????:{0:05d}".format(self.val)
        elif self.val is None:
            return "{0:05d}:?????".format(self.asn)
        else:
            return "{0:05d}:{1:05d}".format(self.asn, self.val)

    @str_value.setter
    def str_value(self, value):
        (self.asn, self.val) = StdValue.parse_value(value, None)

    class Transformer(sqlalchemy.ext.hybrid.Comparator):

        """docstring for Transformer"""

        def __init__(self, cls, asn, val, datetime, join=None):
            super(StdValue.Transformer, self).__init__(None)
            self.cls = cls
            self.asn = asn
            self.val = val
            self.datetime = datetime  # datetime of Ases
            self.join = join

        @property
        def transformer(self):
            if self.join is None:
                def transform(query):
                    return query.order_by(desc(self.datetime))
            else:
                def transform(query):
                    return query.join(self.join
                                      ).order_by(desc(self.datetime))
            return transform

        @property
        def transformer_asc(self):
            if self.join is None:
                def transform(query):
                    return query.order_by(asc(self.datetime))
            else:
                def transform(query):
                    return query.join(self.join
                                      ).order_by(desc(self.datetime))
            return transform

        def __eq__(self, other):
            if isinstance(other, StdValue):
                return ((self.asn == other.asn)
                        & (self.val == other.val)
                        & (self.datetime == other.datetime))
            elif isinstance(other, str):
                (asn, val) = StdValue.parse_value(other)
                return ((self.asn == asn) & (self.val == val))
            else:
                return ((self.asn.op('<<')(16)) + self.val) == other

        def __ne__(self, other):
            if isinstance(other, StdValue):
                return not ((self.asn == other.asn)
                            & (self.val == other.val)
                            & (self.datetime == other.datetime))
            elif isinstance(other, str):
                (asn, val) = StdValue.parse_value(other)
                return not (self.asn == asn & self.val == val)
            else:
                return ((self.asn.op('<<')(16)) + self.val) != other

        def operate(self, op, other):
            if isinstance(other, StdValue):
                return ((self.asn == other.asn)
                        & (self.val == other.val)
                        & op(self.datetime, other.datetime))
            elif isinstance(other, str):
                std_value = StdValue(other)
                return op(
                    ((self.asn.op('<<')(16)) + self.val), std_value.value)
            else:
                return op(((self.asn.op('<<')(16)) + self.val), other)

        def __repr__(self):
            return repr(StdValue(self.asn, self.val, self.datetime))

        def __str__(self):
            return str(StdValue(self.asn, self.val, self.datetime))

    class TransformerRange(Transformer):

        """docstring for TransformerRange

        @deprecated
        """

        def __init__(self, cls, asn, val, datetime, join=None):
            super(StdValue.TransformerRange, self).__init__(
                cls, asn, val, datetime, join)

        def __eq__(self, other):
            if other is None:
                return ((self.cls._from_as_id == self.cls._to_as_id)
                        & (self.cls._from_val == self.cls._to_val))
            else:
                return super().__eq__(other)

        def __ne__(self, other):
            if other is None:
                return ~(self == other)
            else:
                return super().__ne__(other)

    class Comparator(sqlalchemy.ext.hybrid.Comparator):

        """docstring for Comparator"""

        def __init__(self, asn, val):
            super(StdValue.Comparator, self).__init__(None)
            self.asn = asn
            self.val = val

        def __eq__(self, other):
            if isinstance(other, StdValue):
                return (self.asn == other.asn) & (self.val == other.val)
            else:
                std_value = StdValue(other)
                return (self.asn == std_value.asn) & (
                    self.val == std_value.val)

        def __ne__(self, other):
            if isinstance(other, StdValue):
                return (self.asn != other.asn) | (self.val != other.val)
            else:
                std_value = StdValue(other)
                return (self.asn != std_value.asn) | (
                    self.val != std_value.val)

        def operate(self, op, other):
            if isinstance(other, StdValue):
                return op(((self.asn.op('<<')(16)) + self.val), (other.value))
            else:
                std_value = StdValue(other)
                return op(((self.asn.op('<<')(16)) + self.val),
                          (std_value.value))

        def __repr__(self):
            return repr(StdValue(self.asn, self.val))

        def __str__(self):
            return str(StdValue(self.asn, self.val))

    class ComparatorRange(Comparator):

        """docstring for Comparator

        @deprecated
        """

        def __init__(self, to_asn, to_val, from_asn, from_val):
            super(StdValue.ComparatorRange, self).__init__(to_asn, to_val)
            self.from_asn = from_asn
            self.from_val = from_val

        def __eq__(self, other):
            if other is None:
                return (self.from_asn == self.asn) & (
                    self.from_val == self.val)
            elif isinstance(other, StdValue):
                return (self.asn == other.asn) & (self.val == other.val)
            else:
                std_value = StdValue(other)
                return (self.asn == std_value.asn) & (
                    self.val == std_value.val)

        def __ne__(self, other):
            if other is None:
                return (self.from_asn != self.asn) | (
                    self.from_val != self.val)
            elif isinstance(other, StdValue):
                return (self.asn != other.asn) | (self.val != other.val)
            else:
                std_value = StdValue(other)
                return (self.asn != std_value.asn) | (
                    self.val != std_value.val)
